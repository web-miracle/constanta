<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function aboutUs()
    {
        $title = (get_field('about_title') ?? "") ?
                    get_field('about_title') :
                    "О лаборатории";

        $anchore = (get_field('about_anchore') ?? "") ?
                    get_field('about_anchore') :
                    "О лаборатории";

        $info = (get_field('about_info') ?? "") ?
                    get_field('about_info') :
                    "Lorem ipsum dolor sit amet, consectetur ".
                    "adipiscing elit. Ut posuere odio lectus, ".
                    "sit amet eleifend leo commodo sed. ".
                    "Nam mollis et lectus non suscipit. ".
                    "Proin nec tellus quis urna tincidunt ".
                    "faucibus. Phasellus malesuada arcu et ".
                    "erat semper, nec semper sem rutrum. ".
                    "Duis consectetur, diam tempus sodales ".
                    "posuere, augue dolor pretium risus, ".
                    "nec vestibulum nulla ex feugiat nisl. ".
                    "Phasellus a justo urna. Suspendisse ".
                    "luctus feugiat ipsum. Donec id ultrices ".
                    "lectus. Nulla semper nibh luctus luctus ".
                    "vestibulum. Sed vitae diam metus. Duis ".
                    "eget arcu id tortor molestie feugiat in ".
                    "non tellus. In ultricies nisi et sem ".
                    "dictum, in bibendum est lacinia.";

        return [
            "title"   => $title,
            "anchore" => $anchore,
            "info"    => $info
        ];
	}
	
	public function calcModal ()
	{
		return (get_field('calc_modal', 'option') ?? []) ?
				get_field('calc_modal', 'option') :
				$this->defaultcalcModal();
	}

	private function defaultcalcModal() 
	{
		return [
			(object)[
				'post_title' => 'Заказать товар',
				'post_content' => 'Напишите политику для модального окна в настройках'
			]
		];
	}

	public function categories () 
	{
		return (get_field('nom') ?? []) ?
				get_field('nom') :
				$this->defaultCategories();
	}
	
	private function defaultCategories()
	{  
		return [
			[
				"nom_title" => "Вкладки/накладки",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Коронки и мосты",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Прочие работы лаборатории",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Пластины ортодонтические",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Протезы для одноэтапной (базальной)",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Бюгельные протезы",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Съемное протезирование",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			],
			[
				"nom_title" => "Несъемное протезирование на имплантах",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
				],
			[
				"nom_title" => "Безметалловая керамика",
				"nom_products" => [
					[
						"code" => "201",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					],
					[
						"code" => "202",
						"name" => "Коронка цельнолитая CoCr",
						"duration" => "3 д.",
						"price" => "1500"
					]
				]
			]
		];
	}

    public function services()
    {
        $list = (get_field("services_list") ?? []) ?
                    get_field("services_list") :
                    $this->defaultServices();
     
        return $list;
    }

    public function discount () {
        $discount = (get_field('discount_repeater') ?? []) ?
                        get_field('discount_repeater') :
                        $this->defaultDiscount();

        return $discount;
    }

    private function defaultDiscount() {
        return [
            [
                "discount_size" => 3,
                'discount_terms' => 'при заказе от 100 единиц в месяц'
            ],
            [
                'discount_size' => 5,
                'discount_terms' => 'при заказе от 200 единиц в месяц'
            ],
            [
                'discount_size' => 7,
                'discount_terms' => 'при заказе от 300 единиц в месяц'
            ],
        ];
    }

    private function defaultServices()
    {
        return [
            [
                "service_title"  => "Коронки и мосты",
                "service_editor" => ""
            ], [
                "service_title"  => "Безметалловая керамика",
                "service_editor" => ""
            ], [
                "service_title"  => "Несъемное протезирование",
                "service_editor" => ""
            ], [
                "service_title"  => "Съемное протезирование",
                "service_editor" => ""
            ], [
                "service_title"  => "Бюгельные протезы",
                "service_editor" => ""
            ], [
                "service_title"  => "Протезы для базальной имплантации",
                "service_editor" => ""
            ], [
                "service_title"  => "Вклдадки и накладки",
                "service_editor" => ""
            ]
        ];
    }

    public function technology()
    {
        $title = (get_field("technologies_section_title") ?? "") ?
                    get_field("technologies_section_title") :
                    "Какие технологии мы используем";

        $anchore = (get_field("technologies_section_anchore") ?? "") ?
                        get_field("technologies_section_anchore") :
                        "Технологии";

        $list = (get_field("technologies_list") ?? []) ?
                    get_field("technologies_list") :
                    $this->defaultTechnologyList();

        return [
            "title"   => $title,
            "anchore" => $anchore,
            "list"    => $list
        ];
    }

    private function defaultTechnologyList()
    {
        return [
            [
                "tech_title"     => "Восковое моделирование",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"     => "Металлокерамика Shofy",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"   => "Артикуляторы",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"   => "CAD/CAM",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"   => "Виниры",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"   => "Технология e.Max Ivoclar (e.max Press)",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"   => "Технологии наслоения керамических ".
                                  "масс на огнеупорный штампик",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ], [
                "tech_title"   => "3D фрезерование",
                "tech_excerpt" => "Lorem ipsum dolor sit amet, ".
                                  "consectetur adipiscing elit. ".
                                  "Vestibulum non libero sit amet ".
                                  "mauris mollis feugiat.",
                "link"         => "#"
            ]
        ];
    }

    public function worksSection()
    {
        $title = (get_field("works_title") ?? "") ?
                    get_field("works_title") :
                    "Примеры наших работ";

        $anchore = (get_field("works_anchore") ?? "") ?
                        get_field("works_anchore") :
                        "Наши работы";

        return [
            "title"   => $title,
            "anchore" => $anchore
        ];
    }

    public function works()
    {
        return get_field('works_repeater');
    }
}
