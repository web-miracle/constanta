<?php
namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateTech extends Controller
{
    public function aboutUs()
    {
        $title = (get_field('about_title') ?? "") ?
                    get_field('about_title') :
                    "О лаборатории";

        $anchore = (get_field('about_anchore') ?? "") ?
                    get_field('about_anchore') :
                    "О лаборатории";

        $info = (get_field('about_info') ?? "") ?
                    get_field('about_info') :
                    "Lorem ipsum dolor sit amet, consectetur ".
                    "adipiscing elit. Ut posuere odio lectus, ".
                    "sit amet eleifend leo commodo sed. ".
                    "Nam mollis et lectus non suscipit. ".
                    "Proin nec tellus quis urna tincidunt ".
                    "faucibus. Phasellus malesuada arcu et ".
                    "erat semper, nec semper sem rutrum. ".
                    "Duis consectetur, diam tempus sodales ".
                    "posuere, augue dolor pretium risus, ".
                    "nec vestibulum nulla ex feugiat nisl. ".
                    "Phasellus a justo urna. Suspendisse ".
                    "luctus feugiat ipsum. Donec id ultrices ".
                    "lectus. Nulla semper nibh luctus luctus ".
                    "vestibulum. Sed vitae diam metus. Duis ".
                    "eget arcu id tortor molestie feugiat in ".
                    "non tellus. In ultricies nisi et sem ".
                    "dictum, in bibendum est lacinia.";

        return [
            "title"   => $title,
            "anchore" => $anchore,
            "info"    => $info
        ];
	}
	
	public function courseModal()
	{
		return (get_field('course_modal', 'option') ?? []) ?
				get_field('course_modal', 'option') :
				$this->defaultCourseModal();
	}

	private function defaultCourseModal() 
	{
		return [
			(object)[
				'post_title' => 'Записаться на курс',
				'post_content' => 'Напишите политику для модального окна в настройках'
			]
		];
	}

    public function courseList()
    {
        $courses = get_field("course_list") ?? [];
        if (empty($courses)) {
            return $this->defaultCoursesList();
        }

        $courses = array_map(function($course) {
            return (object)[
                "title" => $course->post_title,
                "composition" => get_field("course_composition", $course) ?? [],
                "info" => false,
                "image" => get_the_post_thumbnail_url($course, 'large'),
                "duration" => get_field("course_duration", $course) ?? "",
                "prices" => get_field("course_price", $course) ?? []
            ];
        }, $courses);

        return $courses;
    }

    private function defaultCoursesList() {
        return [
            (object)[
                "title" => "Изготовление металлокерамики",
                "composition" => [
                    ["stage" => "подготовка и изготовление модели"],
                    ["stage" => "изготовление восковой конструкции (мостовидный протез)"],
                    ["stage" => "обработка металлического каркаса"],
                    ["stage" => "нанесение керамической массы"],
                    ["stage" => "раскраска и глазуровка работы"]
                ],
                "info" => false,
                "duration" => "3 дня",
                "prices" => [
                    [
                        "price" => "10 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ],
            (object)[
                "title" => "Изготовление и моделирование диоксида циркония",
                "composition" => [
                    ["stage" => "подготовка и изготовление модели"],
                    ["stage" => "изготовление воскового каркаса"],
                    ["stage" => "работа с циркониевой массой (Profi Zirkon, e.max, ivoclar In Line)"],
                    ["stage" => "раскраска и глазуровка работы"]
                ],
                "info" => false,
                "duration" => "5 дня",
                "prices" => [
                    [
                        "price" => "35 000 ₽",
                        "add_comment" => 1,
                        "comment" => "(Profi Zirkon )"
                    ],
                    [
                        "price" => "40 000  ₽",
                        "add_comment" => 1,
                        "comment" => "(E.Max)"
                    ]
                ]
            ],
            (object)[
                "title" => "Изготовление металлических культевых вкладок",
                "composition" => [
                    ["stage" => "Подготовка слепка"],
                    ["stage" => "Одномоментная отливка модели"],
                    ["stage" => "Выбор, подготовка штифтов под корневой канал"],
                    ["stage" => "Изготовление индивидуальных разборных штифтов"],
                    ["stage" => "Нюансы подготовки восковой композиции перед литьем для сокращения времени обработки, посадки вкладки"],
                    ["stage" => "Посадка вкладки"]
                ],
                "info" => false,
                "duration" => "3 дня",
                "prices" => [
                    [
                        "price" => "7 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ],
            (object)[
                "title" => "Курс по Wax up",
                "composition" => [
                    ["stage" => "Подготовка к работе с воском"],
                    ["stage" => "Соотношение частей формы друг другу и к целому"],
                    ["stage" => "Ориентиры для воссоздания формы зуба"],
                    ["stage" => "Подробный разбор принципа методики скелетного моделирования"],
                    ["stage" => "Создание базы для формы зуба"],
                    ["stage" => "Окклюзионные контакты"],
                    ["stage" => "Воссоздание анатомической формы центральных резцов"],
                    ["stage" => "Воссоздание анатомической формы латеральных резцов"]
                ],
                "info" => false,
                "duration" => "5 дня",
                "prices" => [
                    [
                        "price" => "10 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ],
            (object)[
                "title" => "Курсы с выбором индивидуальной программы обучения",
                "composition" => [
                    ["stage" => "Безметалловая керамика. Изготовление виниров и вкладок по системе e.max"],
                    ["stage" => "Художественное моделирование анатомической формы зуба из керамики"],
                    ["stage" => "Восковое моделирование"],
                    ["stage" => "Техника и основы работы с артикулятором"],
                    ["stage" => "Основы и техника изготовление протезов с опорой на импланты"]
                ],
                "info" => false,
                "duration" => "зависит от выбранной программы",
                "prices" => [
                    [
                        "price" => "зависит от выбранной программы",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ]
        ];
    }

    public function lektor()
    {
        $avatar = (get_field("lektor_avatar") ?? "") ?
                    get_field("lektor_avatar") :
                    false;

        $fio = (get_field("lektor_fio") ?? "") ?
                    get_field("lektor_fio") :
                    "Коровин Юрий Юрьевич";

        $info = (get_field("lektor_info") ?? "") ?
                    get_field("lektor_info") :
                    "Техник высшей категории<br> Опыт работы: 23 года";
        return [
            "lektor_avatar" => $avatar,
            "lektor_fio"    => $fio,
            "lektor_info"   => $info
        ];
    }

    public function postBlog()
    {
        return [[
                "title" => "Название статьи в блоге, выборка по тегу",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "/localhost:3000/app/themes/constanta".
                           "/dist/images/blog_img.jpg"
            ], [
                "title" => "название2",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/constanta".
                           "/dist/images/blog_img.jpg"
            ], [
                "title" => "название3",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/constanta".
                           "/dist/images/blog_img.jpg"
            ]
        ];
    }
}
