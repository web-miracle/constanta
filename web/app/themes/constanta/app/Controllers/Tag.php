<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Tag extends Controller
{
	public function posts() {
		global $wp_query;

		return $wp_query->posts;
	}

	public function tags()
	{
		return get_terms("post_tag", ["hide_empty" => false]);
	}

	public function activeTag()
	{
		global $wp_query;

		return $wp_query->query['tag'];
	}

	public function activeTagObj()
	{
		return get_term_by('slug', $this->activeTag(), 'post_tag');
	}

	public function postCount()
	{
		return $this->activeTagObj()->count;
	}

	public function title()
	{
		return $this->activeTagObj()->name;
	}

	public function desciption()
	{
		$desc = $this->activeTagObj()->description;

		return $desc ? $desc : (get_field("blog-description", "option") ?? "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut posuere odio lectus, sit amet eleifend leo commodo sed. Nam mollis et lectus non suscipit. Proin nec tellus quis urna tincidunt faucibus. Phasellus malesuada arcu et erat semper, nec semper sem rutrum. Duis consectetur, diam tempus sodales posuere, augue dolor pretium risus, nec vestibulum nulla ex feugiat nisl. Phasellus a justo urna. ");
	}
}
