<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{

    public function siteName()
    {
        return get_bloginfo("name");
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option("page_for_posts", true)) {
                return get_the_title($home);
            }
            return __("Latest Posts", "sage");
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__("Search Results for %s", "sage"), get_search_query());
        }
        if (is_404()) {
            return __("Not Found", "sage");
        }
        return get_the_title();
    }

    public function companyInfo()
    {
        return [
            "logo"    => get_field("company-logo", "option"),
            "shedule" => get_field("company-shedule", "option") ??
                         "Без выходных 9:00-18:00",
            "phone"   => get_field("company-phone", "option") ??
                         "8 (800) 2000 600",
            "email"   => get_field("company-email", "option") ??
                         "info@constanta.ru",
            "address" => get_field("company-address", "option") ??
                         "г. Москва<br> ул. Московская, 34Б"
        ];
    }

    public function social()
    {
        return [
            "vk" => get_field("social-vk", "option") ?? "#",
            "telegram" => get_field("social-telegram", "option") ?? "#",
            "instagram" => get_field("social-instagram", "option") ?? "#"
        ];
    }

    public function sertificate()
    {
        return get_field("company-sertificate", "option");
    }

    public function stepWorkSection()
    {
        $title = (get_field("step_work_title", 'option') ?? "") ?
                    get_field("step_work_title", 'option') :
                    "Как мы работаем";

        $anchore = (get_field("step_work_anchore", 'option') ?? "") ?
                        get_field("step_work_anchore", 'option') :
                        "Как мы работаем";

        return [
            "title"   => $title,
            "anchore" => $anchore
        ];
    }

    public function stepWork()
    {
        $res = (get_field("how_works_repeater", "option") ?? []) ?
                    get_field("how_works_repeater", "option") :
                    $this->worksDefault();

        return empty($res) ? $this->worksDefault() : $res;
    }

    private function worksDefault()
    {
        return [
            [
                "how_works_title" => "Онлайн-заказ",
                "how_works_area"  => "Вы оформляете быструю заявку<br> 
                                      или делаете онлайн-расчет заказа"
            ], [
                "how_works_title" => "Подтверждение",
                "how_works_area"  => "Наш менеджер проверяет вашу заявку,<br> 
                                      уточняет необходимую информацию и<br> 
                                      определяет сроки изготовления."
            ], [
                "how_works_title" => "Оплата",
                "how_works_area"  => "Вы можете оплатиь заказ переводом<br> 
                                      на расчетный счет или у нас в офисе."
            ], [
                "how_works_title" => "Изготовление",
                "how_works_area"  => "Если заказ оплачен до 15:00 рабочего дня,<br> 
                                      то он запускается в производство в<br> 
                                      этот же день."
            ], [
                "how_works_title" => "Доставка",
                "how_works_area"  => "Мы доставляем ваш заказ бесплатно в<br> 
                                      пределах МКАД. Доставка работает<br> 
                                      пн-пт с 10:00 до 19:00."
            ]
        ];
    }

    public function blogSection()
    {
        return [
            "title"   => get_field("blog_section_title", "option") ??
                         "Читайте свежее в нашем блоге",
            "anchore" => get_field("blog_section_anchore") ?? "Блог",
            "posts"   => $this->previewPosts()
        ];
    }

    public function previewPosts()
    {
        $terms = get_field("blog_section_taxonomy", "option") ?? [];
        $tags = [];

        if (is_array($terms)) {
            $tags = implode(" ", array_map(function ($term) {
                return $term->slug;
            }, $terms));
        }

        $res = get_posts([
            "numberposts" => 16,
            "tag"         => $tags
        ]);
        return empty($res) ? $this->defaultPostBlog() : $res;
    }

    private function defaultPostBlog()
    {
        return [(object)[
                "post_title"   => "Название статьи в блоге, выборка по тегу",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название2",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название3",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "Название статьи в блоге, выборка по тегу",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название2",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название3",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "Название статьи в блоге, выборка по тегу",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название2",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название3",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "Название статьи в блоге, выборка по тегу",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название2",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ], (object)[
                "post_title"   => "название3",
                "post_excerpt" => "Краткий анонс статьи: какой-то цепляющий текст, 
                                   который привлечет внимание пользователя...",
                "link"         => "#link",
                "post_date"    => "1.02.2018",
                "img"          => "//localhost:3000/app/themes".
                                  "/constanta/dist/images/blog_img.jpg"
            ]
        ];
    }

    public function blogLink()
    {
        return get_post_type_archive_link("post");
	}
	
	public function emailList() 
	{
		$res = (get_field("email_list", "option") ?? []) ?
                    get_field("email_list", "option") :
                    $this->emailDefault();
	}

	private function emailDefault() 
	{
		return [
			[
				'email' => 'info@constanta.ru'
			]
		];
	}

    public function firstScreen()
    {
        if (!is_front_page() &&
            !is_page_template(
                [
                    "views/template-tech.blade.php",
                    "views/template-repair.blade.php",
                    "views/template-make.blade.php"
                ]
            )
        ) {
            return [
                "image"           => "",
                "company_pros"    => $this->companyPros(),
                "header_subtitle" => $this->subTitleConvert(),
                "header_title"    => "",
                "quick_request"   => "",
                "online_request"  => ""
            ];
        }

        $title = (get_field("header_title") ?? "") ?
                    get_field("header_title") :
					$this->defaultHeaderTitle();
					
        $buttons = (get_field("header_buttons") ?? []) ?
                        get_field("header_buttons") :
                        $this->defaultHeaderButtons();

        return [
            "image"           => get_field("header_bg"),
            "company_pros"    => $this->companyPros(),
            "header_subtitle" => $this->subTitleConvert(),
            "header_title"    => $title,
            "buttons"         => $buttons
        ];
    }

    private function defaultHeaderButtons()
    {
        if (is_front_page())
            return [
                [
                    "text" => "Оформить быструю заявку",
					"layout" => "btn--gradient",
					'modal'=> []
                ], [
                    "text" => "Рассчитать заказ онлайн",
					"layout" => "btn--transparent",
					'modal'=> []
                ]
            ];

        if (is_page_template("views/template-tech.blade.php"))
            return [
                [
                    "text" => "Выбрать курс",
					"layout" => "btn--gradient",
					"hash" => "cources",
					'modal'=> []
                ]
            ];

        if (is_page_template("views/template-repair.blade.php"))
            return [
                [
                    "text" => "Получить консультацию",
					"layout" => "btn--gradient",
					'modal'=> []
                ]
			];
			
		if (is_page_template("views/template-make.blade.php"))
            return [
                [
                    "text" => "Выбрать протез",
					"layout" => "btn--gradient",
					'modal'=> [],
					'hash' => 'protez'
                ]
            ];

        return [];
    }

    private function subTitleConvert()
    {
        $subtitle = get_field("header_subtitle");
        if (empty($subtitle)) {
            return $this->defaultHeaderSubTitle();
        }

        $string = explode(" ", get_field("header_subtitle"));
        array_unshift($string, "<span>");
        array_splice($string, 2, 0, array("</span>"));

        return implode(" ", $string);
    }

    private function companyPros()
    {
        $pros = [];
        if (have_rows("company_pros")) :
            while (have_rows("company_pros")) :
                the_row();

                $pros[] = [
                    "image" => get_sub_field("pros_image"),
                    "title" => get_sub_field("pros_title")
                ];
            endwhile;
        endif;

        $pros = array_filter($pros, function($item) {
            if (empty($item["image"]) && empty($item["title"])) {
                return false;
            }

            return true;
        });

        if (!empty($pros)) {
            return $pros;
        }

        return [
            [
                "title" => "Цены ниже, чем у конкурентов.<br> ".
                           "Мы проверяли! :)"
            ], [
                "title" => "Возможно изготовление<br> ".
                           "заказа в этот же день"
            ], [
                "title" => "Сертифицированная<br> ".
                           "лаборатория.<br> ".
                           "Гарантия качества."
            ]
        ];
    }

    private function defaultHeaderTitle()
    {
        if (is_front_page())
            return "Зуботехническая лаборатория";

        if (is_page_template("views/template-tech.blade.php"))
            return "Курсы зубных техников в Москве";

        if (is_page_template("views/template-repair.blade.php"))
            return "Срочный ремонт зубных протезов за 24 часа";

        if (is_page_template("views/template-make.blade.php"))
            return "Срочное изготовление зубных протезов в Москве";

        return "Заголовок шапки";
    }

    private function defaultHeaderSubTitle()
    {
        if (is_front_page())
            return "<span>Срочное</span> изготовление изделий " .
                   "для стоматологий в Москве";

        if (is_page_template("views/template-tech.blade.php"))
            return "Практическое обучение на <span>реальном</span> оборудовании";

        if (is_page_template("views/template-repair.blade.php"))
            return "От 1 500 руб. Высокое качество. Бесплатная доставка.";

        if (is_page_template("views/template-make.blade.php"))
            return "Здесь какой-то еще текст, пока не известно какой";

        return "Подзаголовок шапки";
    }

    public function mapPoint()
    {
        $point = json_decode(get_field("map_point", "option") ?? '[]');
        $point = $point ? $point : $this->defaultMapPoint();

        if (empty($point->marks))
            $point = $this->defaultMapPoint();

        return $point;
    }

    private function defaultMapPoint()
    {
        return (object)[
            "center_lat" => 55.752219999975,
            "center_lng" => 37.61556,
            "zoom" => 14,
            "type" => "map",
            "marks" => [
                (object)[
                    "id" => 1,
                    "content" => "Текст описания",
                    "type" => "Point",
                    "coords" => [55.755221073722, 37.621181910095],
                    "circle_size" => 0
                ]
            ]
        ];
    }
}
