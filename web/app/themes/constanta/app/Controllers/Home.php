<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Home extends Controller
{
	public function posts() {
		global $wp_query;

		return $wp_query->posts;
	}

	public function tags()
	{
		return get_terms("post_tag", ["hide_empty" => false]);
	}

	public function activeTag()
	{
		return null;
	}

	public function title()
	{
		$title = get_field("blog-description", "option") ?? "";

		return $title ? $title : "Блог";
	}
}