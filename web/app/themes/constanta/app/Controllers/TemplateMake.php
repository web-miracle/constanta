<?php
namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateMake extends Controller
{
    public function aboutUs()
    {
        return [
            "title"   => get_field("about_title") ??
                         "О лаборатории",
            "anchore" => get_field("about_anchore") ??
                         "О лаборатории",
            "info"    => get_field("about_info") ??
                         "Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                          Ut posuere odio lectus, sit amet eleifend leo commodo sed. 
                          Nam mollis et lectus non suscipit. Proin nec tellus quis 
                          urna tincidunt faucibus. Phasellus malesuada arcu et 
                          erat semper, nec semper sem rutrum. Duis consectetur, 
                          diam tempus sodales posuere, augue dolor pretium risus, 
                          nec vestibulum nulla ex feugiat nisl. Phasellus a justo urna. 
                          Suspendisse luctus feugiat ipsum. Donec id ultrices lectus. 
                          Nulla semper nibh luctus luctus vestibulum. Sed vitae diam 
                          metus. Duis eget arcu id tortor molestie feugiat in non 
                          tellus. In ultricies nisi et sem dictum, in bibendum est 
                          lacinia."
        ];
    }

    public function services()
    {
        return [];
    }

    public function courseList()
    {
        $courses = get_field("course_list") ?? [];
        if (empty($courses)) {
            return $this->defaultProtezList();
        }

        $courses = array_map(function($course) {
            return (object)[
                "title" => $course->post_title,
                "composition" => false,
                "info" => $course->post_content,
                "image" => get_the_post_thumbnail_url($course, 'large'),
                "duration" => get_field("course_duration", $course) ?? "",
                "prices" => get_field("course_price", $course) ?? []
            ];
        }, $courses);

        return $courses;
    }

    private function defaultProtezList()
    {
        return [
            (object)[
                "title" => "Акриловые протезы Acry-Free",
                "composition" => false,
                "info" => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                           Vestibulum non libero sit amet mauris mollis feugiat. 
                           Integer vulputate a ipsum sit amet rhoncus. Vestibulum nec 
                           ultricies turpis. Etiam tempus condimentum vulputate. 
                           Sed eu congue odio.</p>
                           <p>Nunc sodales purus id augue pulvinar, a tristique 
                           mauris condimentum. Cras dignissim sit amet diam vitae 
                           maximus. Sed efficitur ipsum nec ipsum dictum ornar 
                           condimentum vulputate. Sed eu congue odio. Nunc sodales 
                           purus id augue pulvinar, a tristique mauris condimentum. 
                           Cras Sed eu congue odio. Nunc sodales purus id augue 
                           pulvinar, a tristique mauris.</p>",
                "duration" => "5-8 дня",
                "prices" => [
                    [
                        "price" => "от 25 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ], (object)[
                "title" => "Бюгельные протезы",
                "composition" => false,
                "info" => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                           Vestibulum non libero sit amet mauris mollis feugiat. 
                           Integer vulputate a ipsum sit amet rhoncus. Vestibulum 
                           nec ultricies turpis. Etiam tempus condimentum vulputate. 
                           Sed eu congue odio.</p>
                           <p>Nunc sodales purus id augue pulvinar, a tristique 
                           mauris condimentum. Cras dignissim sit amet diam vitae 
                           maximus. Sed efficitur ipsum nec ipsum dictum ornar 
                           condimentum vulputate. Sed eu congue odio. Nunc sodales 
                           purus id augue pulvinar, a tristique mauris condimentum. 
                           Cras Sed eu congue odio. Nunc sodales purus id augue 
                           pulvinar, a tristique mauris.</p>",
                "duration" => "5-8 дня",
                "prices" => [
                    [
                        "price" => "от 25 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ], (object)[
                "title" => "Полный съемный протез",
                "composition" => false,
                "info" => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                           Vestibulum non libero sit amet mauris mollis feugiat. 
                           Integer vulputate a ipsum sit amet rhoncus. Vestibulum nec 
                           ultricies turpis. Etiam tempus condimentum vulputate. 
                           Sed eu congue odio.</p>
                           <p>Nunc sodales purus id augue pulvinar, a tristique mauris 
                           condimentum. Cras dignissim sit amet diam vitae maximus. 
                           Sed efficitur ipsum nec ipsum dictum ornar condimentum 
                           vulputate. Sed eu congue odio. Nunc sodales purus id augue 
                           pulvinar, a tristique mauris condimentum. Cras Sed eu 
                           congue odio. Nunc sodales purus id augue pulvinar, 
                           a tristique mauris.</p>",
                "duration" => "5-8 дня",
                "prices" => [
                    [
                        "price" => "от 25 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ], (object)[
                "title" => "ВИНИР",
                "composition" => false,
                "info" => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                           Vestibulum non libero sit amet mauris mollis feugiat. 
                           Integer vulputate a ipsum sit amet rhoncus. Vestibulum 
                           nec ultricies turpis. Etiam tempus condimentum vulputate. 
                           Sed eu congue odio.</p>
                           <p>Nunc sodales purus id augue pulvinar, a tristique mauris 
                           condimentum. Cras dignissim sit amet diam vitae maximus. 
                           Sed efficitur ipsum nec ipsum dictum ornar condimentum 
                           vulputate. Sed eu congue odio. Nunc sodales purus id augue 
                           pulvinar, a tristique mauris condimentum. Cras Sed eu congue 
                           odio. Nunc sodales purus id augue pulvinar, 
                           a tristique mauris.</p>",
                "duration" => "5-8 дня",
                "prices" => [
                    [
                        "price" => "от 25 000 ₽",
                        "add_comment" => 0,
                        "comment" => ""
                    ]
                ]
            ]
        ];
	}
	
	public function courseModal()
	{
		return (get_field('Protez_modal', 'option') ?? []) ?
				get_field('Protez_modal', 'option') :
				$this->defaultProtezModal();
	}

	private function defaultProtezModal() 
	{
		return [
			(object)[
				'post_title' => 'Получить консультацию по протезам',
				'post_content' => 'Напишите политику для модального окна в настройках'
			]
		];
	}

    public function worksSection()
    {
        $title = (get_field("works_title") ?? "") ?
                    get_field("works_title") :
                    "Примеры наших работ";

        $anchore = (get_field("works_anchore") ?? "") ?
                        get_field("works_anchore") :
                        "Наши работы";

        return [
            "title"   => $title,
            "anchore" => $anchore
        ];
    }

    public function works()
    {
        return get_field("works_repeater");
    }

    public function postBlog()
    {
        return [[
                "title" => "Название статьи в блоге, выборка по тегу",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/constanta/".
                           "dist/images/blog_img.jpg"
            ],  [
                "title" => "название2",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/constanta/".
                           "dist/images/blog_img.jpg"
            ],  [
                "title" => "название3",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/constanta/".
                           "dist/images/blog_img.jpg"
            ]
        ];
    }
}
