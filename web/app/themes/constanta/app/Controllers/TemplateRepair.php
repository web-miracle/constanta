<?php
namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateRepair extends Controller
{
    public function recomendation()
    {
        $title = (get_field("recom_title") ?? "") ?
                    get_field("recom_title") :
                    "Рекомендуем вам отремонтировать протез, если:";

        $anchore = (get_field("recom_anchore") ?? "") ?
                        get_field("recom_anchore") :
                        "Рекомендуем отремонтировать";

        $image = get_field("recom_image") ?? null;

        $list = (get_field("recom_list") ?? []) ?
                    get_field("recom_list") :
                    $this->defaultRecomList();

        return [
            "title"   => $title,
            "anchore" => $anchore,
            "image"   => $image,
            "list"    => $list
        ];
	}

    private function defaultRecomList()
    {
		return [
			[
				"recom_list_title" => "Вы заметили, что цвет конструкции изменился"
            ], [
				"recom_list_title" => "Отсоединилась небольшая часть зубного 
				протеза, произошло отслоение или откол"
            ], [
				"recom_list_title" => "Вы испытываете неприятные ощущения или 
				дискомфорт при ношении зубного протеза, 
				которого раньше не наблюдалось"
            ], [
				"recom_list_title" => "Произошло механическое повреждение 
				механического протеза (по разным причинам)"
				]
			];
	}
		
	public function repairModal()
	{
		return (get_field('repair_modal', 'option') ?? []) ?
				get_field('repair_modal', 'option') :
				$this->defaultRepairModal();
	}

	private function defaultRepairModal() 
	{
		return [
			(object)[
				'post_title' => 'Хочу отремонтировать протез',
				'post_content' => 'Напишите политику для модального окна в настройках'
			]
		];
	}

    public function reason()
    {
        $title = (get_field("reason_title") ?? "") ?
                    get_field("reason_title") :
                    "Почему протез ломается?";

        $anchore = (get_field("reason_anchore") ?? "") ?
                        get_field("reason_anchore") :
                        "Почему протез ломается?";

        $subtitle = (get_field("reason_subtitle") ?? "") ?
                        get_field("reason_subtitle") :
                        "Наиболее частые причины поломки";

        $list = (get_field("reason_list") ?? []) ?
                    get_field("reason_list") :
                    $this->defaultReasonList();
        return [
            "title"    => $title,
            "anchore"  => $anchore,
            "subtitle" => $subtitle,
            "list"     => $list
        ];
    }

    private function defaultReasonList()
    {
        return [
            [
                "reason_list_title" => "Низкое качество протеза, плохие показатели
                                        стойкости к механическому воздействию"
            ], [
                "reason_list_title" => "Каркас базиса устарел и потерял свои 
                                        первоначальные свойства"
            ], [
                "reason_list_title" => "Плохая гигиена ротовой полости"
            ], [
                "reason_list_title" => "Пандение протеза, удар о твердый предмет"
            ], [
                "reason_list_title" => "Неправильная эксплуатация конструкции"
            ], [
                "reason_list_title" => "Потеря опрной части для крепления"
            ], [
                "reason_list_title" => "Нарушение технологии изготовления 
                                        искусственной детали ряда"
            ], [
                "reason_list_title" => "Неверно подобранный состав элементов"
            ]
        ];
    }

    public function repair()
    {
        $title = (get_field("repair_title") ?? "") ?
                    get_field("repair_title") :
                    "Как происходит починка протеза?";

        $anchore = (get_field("repair_anchore") ?? "") ?
                        get_field("repair_anchore") :
                        "Почему протеза";

        $list = (get_field("repair_list") ?? []) ?
                    get_field("repair_list") :
                    $this->defaultRepairList();

        return [
            "title"   => $title,
            "anchore" => $anchore,
            "list"    => $list
        ];
    }

    private function defaultRepairList()
    {
        return [
            [
                "repair_list_title" => "Для починок используется полимеризационный 
                                        аппарат высокого давления."
            ], [
                "repair_list_title" => "Отсутствующий искусственный зуб наращивают 
                                        (пластик) или вживляют (керамика) под 
                                        воздействием высоких температур."
            ], [
                "repair_list_title" => "При ремонте бюгельных конструкций с 
                                        металлическими составляющими, искусственные десну 
                                        и зубы снимают с дуги. Затем спаивают 
                                        лазером кламмеры или основу."
            ], [
                "repair_list_title" => "Поврежденные части нейлонового протеза 
                                        соединяют при помощи специальных 
                                        скрепляющих жидкостей."
            ]
        ];
    }

    public function price()
    {
        $title = (get_field("price_title") ?? "") ?
                    get_field("price_title") :
                    "Сколько стоит ремонт протеза?";

        $anchore = (get_field("price_anchore") ?? "") ?
                        get_field("price_anchore") :
                        "Стоимость ремонта";

        $info = (get_field("price_info") ?? "") ?
                    get_field("price_info") :
                    "Наша лаборатория проводит следующие виды ремонта:<br>
                    - перелом протеза<br>
                    - частичная приварка зуба";

        $list = (get_field("price_list") ?? []) ?
                    get_field("price_list") :
                    $this->defaultPriceList();

        return [
            "title"   => $title,
            "anchore" => $anchore,
            "info"    => $info,
            "list"    => array_chunk($list, 2, true)
        ];
    }

    private function defaultPriceList()
    {
        return [
            [
                "price_list_title" => "Стоимость простой починки",
                "price_list_price" => "1 500 ₽"
            ], [
                "price_list_title" => "Стоимость сложной починки",
                "price_list_price" => "2 500 ₽"
            ]
        ];
    }

    // public function stepWork()
    // {
    //     return get_field("how_works_repeater", "option") ?? $this->works_default();
    // }

    public function postBlog()
    {
        return [[
                "title" => "Название статьи в блоге, выборка по тегу",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст,
                           который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/".
                           "constanta/dist/images/blog_img.jpg"
            ],  [
                "title" => "название2",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/".
                           "constanta/dist/images/blog_img.jpg"
            ],  [
                "title" => "название3",
                "desc"  => "Краткий анонс статьи: какой-то цепляющий текст, 
                            который привлечет внимание пользователя...",
                "link"  => "#link",
                "date"  => "1.02.2018",
                "img"   => "//localhost:3000/app/themes/".
                           "constanta/dist/images/blog_img.jpg"
            ]
        ];
    }
}
