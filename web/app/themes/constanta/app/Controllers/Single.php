<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Single extends Controller
{
	public function post() {
		global $wp_query;

		return $wp_query->posts[0];
	}

	public function otherPosts()
	{
		return get_posts([
			"numberposts" => 5,
			"orderby" => "rand"
		]);
	}

	public function shared()
	{
		$link = urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$title = urlencode($this->post()->post_title);
		
		$vk = "https://vk.com/share.php?url=$link&title=$title&utm_source=share2";

		$facebook = "https://www.facebook.com/sharer.php?src=sp&u=$link&title=$title&utm_source=share2";

		$telegram = "https://telegram.me/share/url?url=$link&text=$title&utm_source=share2";

		$twitter = "https://twitter.com/intent/tweet?text=$title&url=$link&utm_source=share2";

		$google = "https://plus.google.com/share?url=$link&utm_source=share2";

		$livejournal = "http://www.livejournal.com/update.bml?subject=$title&event=$link";

		return (object)[
			"vk"          => $vk,
			"facebook"    => $facebook,
			"telegram"    => $telegram,
			"twitter"     => $twitter,
			"google"      => $google,
			"livejournal" => $livejournal
		];
	}
}
