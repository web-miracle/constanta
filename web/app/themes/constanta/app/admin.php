<?php

namespace App;

/**
 * Theme customizer
 */
add_action("customize_register", function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting("blogname")->transport = "postMessage";
    $wp_customize->selective_refresh->add_partial("blogname", [
        "selector" => ".brand",
        "render_callback" => function () {
            bloginfo("name");
        }
    ]);
});

/**
 * Customizer JS
 */
add_action("customize_preview_init", function () {
    wp_enqueue_script("sage/customizer.js", asset_path("scripts/customizer.js"), ["customize-preview"], null, true);
});

if (function_exists("acf_add_options_page")) {
    acf_add_options_page([
        "page_title" => "Общие настройки",
        "menu_slug" => "company_setting",
        "position" => 2,
        "icon_url" => "dashicons-schedule"
    ]);
}

/**
 * Register cours post
 */
add_action('init', function() {
    register_post_type('cours', [
        'labels'               => [
            'name'                 => 'Курсы',
            'singular_name'        => 'Курс',
            'menu_name'            => 'Курсы',
            'name_admin_bar'       => 'Курсы',
            'add_new'              => 'Добавить в курсы',
            'add_new_item'         => 'Добавить курс',
            'new_item'             => 'Новый курс',
            'edit_item'            => 'Редактировать курс',
            'view_item'            => 'Посмотреть',
            'all_items'            => 'Все курсы',
            'search_items'         => 'Поиск курсов',
            'parent_item_colon'    => 'Родительский курс',
            'not_found'            => 'Нет результатов',
            'not_found_in_trash'   => 'Ничего не найдено в корзине',
            'attributes'           => 'Page Attributes',
            'featured_image'        => 'Изображение курса',
            'set_featured_image'    => 'Установить изображение курса',
            'remove_featured_image' => 'Убрать изображение',
            'use_featured_image'    => 'Использовать как изображение реквизита',
        ],
        'description'          => '',
        'public'               => false,
        'exclude_from_search'  => false,
        'publicly_queryable'   => true,
        'show_ui'              => true,
        'show_in_menu'         => true,
        'show_in_nav_menus'    => true,
        'show_in_admin_bar'    => true,
        'menu_position'        => 11,
        'menu_icon'            => 'dashicons-clipboard',
        'capability_type'      => 'post',
        'has_archive'          => false,
        'hierarchical'         => false,
        'supports'             => ['title', 'editor', 'thumbnail'],
        'taxonomies'           => []
    ]);
    add_theme_support('post-thumbnails', ['cours']);
});


/**
 * Register modals post
 */
add_action('init', function() {
    register_post_type('modals', [
        'labels'               => [
            'name'                 => 'Модальные окна',
            'singular_name'        => 'Модальное окно',
            'menu_name'            => 'Модальные окна',
            'name_admin_bar'       => 'Модальные окна',
            'add_new'              => 'Добавить окно',
            'add_new_item'         => 'Добавить модальное окно',
            'new_item'             => 'Новое модальное окно',
            'edit_item'            => 'Редактировать модальное окно',
            'view_item'            => 'Посмотреть',
            'all_items'            => 'Все модальные окна',
            'search_items'         => 'Поиск модальных окон',
            'parent_item_colon'    => 'Родительское модальное окно',
            'not_found'            => 'Нет результатов',
            'not_found_in_trash'   => 'Ничего не найдено в корзине',
            'attributes'           => 'Page Attributes',
        ],
        'description'          => '',
        'public'               => false,
        'exclude_from_search'  => false,
        'publicly_queryable'   => true,
        'show_ui'              => true,
        'show_in_menu'         => true,
        'show_in_nav_menus'    => true,
        'show_in_admin_bar'    => true,
        'menu_position'        => 11,
        'menu_icon'            => 'dashicons-grid-view',
        'capability_type'      => 'post',
        'has_archive'          => false,
        'hierarchical'         => false,
        'supports'             => ['title', 'editor'],
        'taxonomies'           => []
    ]);
});

add_action('wp_ajax_add_posts', __NAMESPACE__ . '\\added_posts');
add_action('wp_ajax_nopriv_add_posts', __NAMESPACE__ . '\\added_posts');

function added_posts() {
	if( ! wp_verify_nonce( $_POST['nonce'], 'constanta' ) ) die( 'Stop!');
    $posts = get_posts([
        "number_posts" => $_POST["per_page"],
        "offset"       => $_POST["offset"],
        "tag"          => $_POST["tags"]
    ]);

    $response = "";

    foreach ($posts as $post) {
        $response .= Template("partials.blog.card", [
            "mixin" => "blog-item_shadow blog-page__item",
            "post" => $post
        ]);
    }

    echo $response;

    wp_die();
}

add_action('wp_ajax_send_mail', __NAMESPACE__ . '\\send_mail');
add_action('wp_ajax_nopriv_send_mail', __NAMESPACE__ . '\\send_mail');

function send_mail() {
	if( ! wp_verify_nonce( $_POST['nonce'], 'constanta' ) ) die( 'Stop!');
	$subject = 'Заявка с сайта константа';
	$email_list = get_field("email_list", "option");
	$formName = $_POST['formName'];
	$name = $_POST['name'];
	$tel = $_POST['tel'];
	$data = $_POST['data'];
	$headers = 'From: Ваш сайт <site@constanta>' . "\r\n";
	$message = "Название формы: $formName \r\n Имя человека: $name \r\n Телефон: $tel \r\n Информация от клиента: $data";
	$send = [];
	foreach($email_list as $email) {
		$send[] = wp_mail($email, $subject, $message, $headers );
	}
	if ($send) echo json_encode($send);
	else echo 'false';

	wp_die();
}