<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("How_works", ["title" => "Порядок нашей работы"]);

$post
    ->setLocation("options_page", "==", "company_setting");

$post
    ->addText("step_work_title", [
        "label" => "Заголовок раздела"
    ])
    ->addText("step_work_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addRepeater("how_works_repeater", [
        "label" => "Порядок работы",
        "min" => 0,
        "button_label" => "Добавить этап",
        "layout" => "row"
    ])
        ->addText("how_works_title", [
            "label" => "Заголовок этапа",
        ])
        ->addTextArea("how_works_area", [
            "label" => "Описание этапа"
        ]);

return $post;
