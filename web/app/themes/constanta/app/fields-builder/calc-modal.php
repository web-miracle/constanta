<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("calc-modal", ["title" => "Настройка модального окна для калькулятора"]);

$post
	->setLocation("options_page", "==", "company_setting");
	
$post
	->addRelationship('calc_modal', [
		"label" => "Модальное окно для калькулятора",
		"post_type" => ["modals"],
		"taxonomy" => [],
		"filters" => ["search"],
		"min" => 1,
		'max' => 1,
		"return_format" => "object",
	]);

return $post;