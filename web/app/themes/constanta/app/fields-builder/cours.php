<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("Course-info", ["title" => "Информация о курсе"]);

$post
    ->setLocation("post_type", "==", "cours");

$post
    ->addRepeater("course_composition", [
        "label" => "Курс состоит",
        "min" => 0,
        "button_label" => "Добавить этап курса",
        "layout" => "row"
    ])
        ->addText("stage", [
            "label" => "Этап"
        ])
        ->endRepeater()
    ->addText("course_duration", [
        "label" => "Длительность курса"
    ])
    ->addRepeater("course_price", [
        "label" => "Стоимость курса",
        "button_label" => "Добавить цену",
        "layout" => "table"
    ])
        ->addText("price", [
            "label" => "Цена курса"
        ])
        ->addTrueFalse("add_comment", [
            "label" => "Добавить комментарий"
        ])
        ->addText("comment", [
            "label" => "Пометка к цене"
        ])
            ->conditional("add_comment", "==", "1");

return $post;
