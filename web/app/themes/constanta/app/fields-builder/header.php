<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("Shapka", ["title" => "Шапка"]);

$post
    ->setLocation("page_template", "==", "views/template-tech.blade.php")
        ->or("page_template", "==", "views/template-make.blade.php")
        ->or("page_template", "==", "views/template-repair.blade.php")
        ->or("page_type", "==", "front_page");

$post
    ->addImage("header_bg", [
        "label" => "Фоновое изображение",
        "required" => 1
    ])
    ->addText("header_title", [
        "label" => "Заголовок",
        "placeholder" => "Введите заголовок"
    ])
    ->addText("header_subtitle", [
        "label" => "Подзаголовок",
        "placeholder" => "Введите подзаголовок"
    ])

    ->addRepeater("header_buttons", [
        "label" => "Кнопки в шапке",
        "button_label" => "Добавить кнопку",
		"layout" => "row",
		'max' => 2
    ])
        ->addText("text", [
            "label" => "Текст кнопки",
            "placeholder" => "Введите текст",
            "wrapper" => [
                "width" => "20"
            ]
        ])
        ->addSelect("layout", [
            "label" => "Внешний вид кнопки",
            "choices" => [
                "btn--transparent" => "Прозрачная",
                "btn--gradient" => "Залитая"
            ],
            "return_format" => "value",
            "wrapper" => [
                "width" => "15"
            ]
		])
		->addSelect("type", [
            "label" => "Тип кнопки",
            "choices" => [
                "modal" => "Модальное окно",
                "hash" => "Якорная ссылка"
            ],
            "return_format" => "value",
            "wrapper" => [
                "width" => "15"
            ]
		])
		->addRelationship("modal", [
			"label" => "Модальное окно",
			"post_type" => ["modals"],
			"taxonomy" => [],
			"filters" => ["search"],
			"min" => 1,
			'max' => 1,
			"return_format" => "object",
			"wrapper" => [
                "width" => "50"
            ]
		])
			->conditional('type', '==', 'modal')
        ->endRepeater()
    // ->addText("quick_request", [
    //     "label" => "Текст для кнопки быстрой заявки",
    //     "placeholder" => "Введите текст"
    // ])
    // ->addText("online_request", [
    //     "label" => "Текст для кнопки заявки онлайн",
    //     "placeholder" => "Введите текст"
    // ])
    

    ->addRepeater("company_pros", [
        "label" => "Преимущества компании",
        "min" => 1,
        "max" => 3,
        "button_label" => "Добавить преимущество",
        "layout" => "table",
    ])
        ->addImage("pros_image", [
            "label" => "Иконка преимущества",
            "preview_size" => "medium"
        ])
        ->addTextArea("pros_title", [
            "label" => "Заголовок преимущества",
            "placeholder" => "Введите текст",
            "new_lines" => "br",
            "rows" => ""
        ]);

return $post;
