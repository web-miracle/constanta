<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('Logo', ["title" => "Логотип"]);

$post
    ->setLocation('options_page', '==', 'company_setting');
  
$post
    ->addImage("company-logo", ["label" => "Логотип"]);

return $post;
