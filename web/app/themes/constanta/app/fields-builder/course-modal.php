<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("course-modal", ["title" => "Настройка модального окна для курсов"]);

$post
	->setLocation("options_page", "==", "company_setting");
	
$post
	->addRelationship('course_modal', [
		"label" => "Модальное окно для курсов",
		"post_type" => ["modals"],
		"taxonomy" => [],
		"filters" => ["search"],
		"min" => 1,
		'max' => 1,
		"return_format" => "object",
	]);

return $post;