<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('About_us', ["title" => "О лаборатории"]);

$post
    ->setLocation('page_type', '==', 'front_page')
    ->or("page_template", "==", "views/template-tech.blade.php");

$post
    ->addText("about_title", [
        "label" => "Заголовок раздела"
    ])
    ->addText("about_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addTextArea("about_info", [
        "label" => "Описание лаборатории"
    ]);

return $post;
