<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("repair-modal", ["title" => "Настройка модального окна для ремонта"]);

$post
	->setLocation("options_page", "==", "company_setting");
	
$post
	->addRelationship('repair_modal', [
		"label" => "Модальное окно для ремонта",
		"post_type" => ["modals"],
		"taxonomy" => [],
		"filters" => ["search"],
		"min" => 1,
		'max' => 1,
		"return_format" => "object",
	]);

return $post;