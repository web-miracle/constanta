<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('Info', ["title" => "Информация о компании"]);

$post->setLocation('options_page', '==', 'company_setting');

$post
    ->addText(
        "company-shedule",
        [
            "label" => "График работы",
            "placeholder" => "Без выходных 9:00-18:00"
        ]
    )
    ->addText(
        "company-phone",
        [
            "label" => "Номер телефона для связи",
            "placeholder" => "8 (xxx) xxxx xxx"
        ]
    )
    ->addEmail(
        "company-email",
        [
            "label" => "Почтовый ящик для обратной связи",
            "placeholder" => "info@constanta.ru"
        ]
    )
    ->addTextarea(
        "company-address",
        [
            "label" => "Адрес компании",
            "placeholder" => "г. Москва ул. Московская, 34Б",
            'rows' => 2,
            'new_lines' => 'br'
        ]
    )
    ->addGallery(
        "company-sertificate",
        [
            "label" => "Сертификаты компании"
        ]
    );

return $post;
