<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("reason", ["title" => "Почему протезы ломаются"]);

$post
    ->setLocation("page_template", "==", "views/template-repair.blade.php");

$post
    ->addText("reason_title", [
        "label" => "Заголовок блока"
    ])
    ->addText("reason_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addText("reason_subtitle", [
        "label" => "Ползаголовок блока"
    ])
    ->addRepeater("reason_list", [
        "label" => "Список причин",
        "min" => 0,
        "button_label" => "Добавить причину",
        "layout" => "row",
    ])
        ->addText("reason_list_title", [
            "label" => "Название причины"
        ]);

return $post;
