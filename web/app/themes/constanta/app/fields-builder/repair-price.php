<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("price", ["title" => "Цена ремонта"]);

$post
    ->setLocation("page_template", "==", "views/template-repair.blade.php");

$post
    ->addText("price_title", [
        "label" => "Заголовок блока"
    ])
    ->addText('price_anchore', [
        'label' => 'Текст в якорной ссылке'
    ])
    ->addTextArea("price_info", [
        "label" => "Описание блока",
        "new_lines" => "br"
    ])
    ->addRepeater("price_list", [
        "label" => "Варианты ремонта",
        "min" => 0,
        "max" => 4,
        "button_label" => "Добавить вариант",
        "layout" => "table",
    ])
        ->addText("price_list_title", [
            "label" => "Название ремонта"
        ])
        ->addText("price_list_price", [
            "label" => "Цена ремонта"
        ]);

return $post;
