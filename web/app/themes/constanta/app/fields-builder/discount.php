<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("discount", ["title" => "Скидки"]);

$post
    ->setLocation("options_page", "==", "company_setting");

$post
    ->addRepeater("discount_repeater", [
        "label" => "Перечень скидок",
		"min" => 0,
		'max' => 3,
        "button_label" => "Добавить скидку",
        "layout" => "row"
    ])
        ->addNumber("discount_size", [
            "label" => "Размер скидки"
		])
		->addText('discount_terms', [
			'label' => 'Условия скидки'
		]);

return $post;
