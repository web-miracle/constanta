<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("protez-modal", ["title" => "Настройка модального окна для протезов"]);

$post
	->setLocation("options_page", "==", "company_setting");
	
$post
	->addRelationship('protez_modal', [
		"label" => "Модальное окно для протезов",
		"post_type" => ["modals"],
		"taxonomy" => [],
		"filters" => ["search"],
		"min" => 1,
		'max' => 1,
		"return_format" => "object",
	]);

return $post;