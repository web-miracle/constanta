<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('tag_name_settings', ["title" => "Короткое название"]);

$post
    ->setLocation('taxonomy', '==', 'post_tag');

$post
    ->addText("short_tag_name", [
        "label" => "Короткое название тега"
    ]);

return $post;
