<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('company-social-link', ["title" => 'Социальные сети компании']);

$post->setLocation('options_page', '==', 'company_setting');

$post
    ->addUrl(
        "social-vk",
        [
            "label" => "Вконтакте",
            "required" => 1
        ]
    )
    ->addUrl(
        "social-telegram",
        [
            "label" => "Telegram",
            "required" => 1
        ]
    )
    ->addUrl(
        "social-instagram",
        [
            "label" => "Instagram",
            "required" => 1
        ]
    );

return $post;
