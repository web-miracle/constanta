<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("protez-list", ["title" => "Протезы"]);

$post
    ->setLocation("page_template", "==", "views/template-make.blade.php");

$post
    ->addRepeater("protez_list_repeater", [
        "label" => "Список протезов",
        "min" => 0,
        "button_label" => "Добавить протез",
        "layout" => "row",
    ])
        ->addImage("course_avatar", [
            "label" => "Фото протеза",
            "preview_size" => "medium"
        ])
        ->addText("course_title", [
            "label" => "Название протеза"
        ])
        ->addWysiwyg("course_info", [
            "label" => "Описание протеза"
        ])
        ->addText("course_duration", [
            "label" => "Длительность изготовления протеза"
        ])
        ->addRepeater("course_price_repeater", [
            "label" => "Стоимость протеза",
            "button_label" => "Добавить ",
            "layout" => "table"
        ])
            ->addText("course_price", [
                "label" => "Цена протеза"
            ])
            ->addTrueFalse("add_comment", [
                "label" => "Добавить комментарий"
            ])
            ->addText("course_price_comment", [
                "label" => "Пометка к цене"
            ])
                ->conditional("add_comment", "==", "1");

return $post;
