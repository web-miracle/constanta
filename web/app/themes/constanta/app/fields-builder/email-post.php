<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('email-post', ["title" => "Настройка отправки писем"]);

$post
    ->setLocation('options_page', '==', 'company_setting');
  
$post
    ->addRepeater('email_list', [
		'label' => 'Перечень адресатов',
		'min' => 1,
		'button_label' => 'Добавить адресата'
	])
		->addEmail('email', [
			'label' => 'Адресат'
		]);

return $post;
