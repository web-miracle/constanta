<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('blog_setting_option', ["title" => "Страница блога"]);

$post->setLocation('options_page', '==', 'company_setting');

$post
    ->addText("blog-title", [
        "label" => "Заголовок блога"
    ])
    ->addText("blog-description", [
        "label" => "Описание на странице",
    ]);

return $post;
