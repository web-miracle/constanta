<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('Technologies', ["title" => "Технологии"]);

$post
    ->setLocation('page_type', '==', 'front_page');

$post
    ->addText("technologies_section_title", [
        "label" => "Заголовок раздела"
    ])
    ->addText("technologies_section_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addRepeater("technologies_list", [
        "label" => "Список технологий",
        "min" => 0,
        "button_label" => "Добавить технологию",
        "layout" => "block",
    ])
        ->addText("tech_title", [
            "label" => "Название технологии"
        ])
        ->addTextArea("tech_excerpt", [
            "label" => "Короткое описание технологии"
        ])
        ->addWysiwyg("service_editor", [
            "label" => "Описание технологии"
        ]);

return $post;
