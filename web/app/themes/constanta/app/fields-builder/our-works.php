<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('Our-works', ["title" => "Наши работы"]);

$post
    ->setLocation('page_type', '==', 'front_page')
        ->or('page_template', '==', 'views/template-make.blade.php');

$post
    ->addText("works_title", [
        "label" => "Заголовок раздела"
    ])
    ->addText("works_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addRepeater("works_repeater", [
        "label" => "Перечень работ",
        "min" => 0,
        "button_label" => "Добавить работу",
        "layout" => "row"
    ])
        ->addImage("about_image", [
            "label" => "Изображение работы",
            "preview_size" => "medium"
        ]);

return $post;
