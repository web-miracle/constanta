<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("Course-mentor", ["title" => "Курс проводит"]);

$post
    ->setLocation("page_template", "==", "views/template-tech.blade.php");

$post
    ->addImage("lektor_avatar", [
        "label" => "Фото лектора",
        "preview_size" => "medium"
    ])
    ->addText("lektor_fio", [
        "label" => "ФИО лектора"
    ])
    ->addTextArea("lektor_info", [
        "label" => "Описание лектора",
        "new_lines" => "br"
    ]);

return $post;
