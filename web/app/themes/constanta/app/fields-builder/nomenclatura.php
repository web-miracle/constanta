<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("nomenclatura", ["title" => "Номенклатура"]);

$post
    ->setLocation("page_template", "==", "default");

$post
    ->addRepeater('nom',[
		'label' => 'Перечень категорий',
		'min' => 0,
		'layout' => 'row',
		"button_label" => "Добавить категорию",
	])
		->addText('nom_title', [
			'label' => 'Название категории'
		])
		->addRepeater('nom_products',[
			'label' => 'Товары',
			'min' => 0,
			'layout' => 'table',
			"button_label" => "Добавить товар",
		])
			->addText('code', [
				'label' => 'Код товара'
			])
			->addText('name', [
				'label' => 'Название'
			])
			->addText('duration', [
				'label' => 'Сроки изготовления'
			])
			->addText('price', [
				'label' => 'Стоимость изготовления'
			]);

return $post;
