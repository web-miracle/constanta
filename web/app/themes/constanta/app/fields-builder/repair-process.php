<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('repair', ["title" => "Как происходит починка"]);

$post
    ->setLocation('page_template', '==', 'views/template-repair.blade.php');

$post
    ->addText('repair_title', [
        'label' => 'Заголовок блока'
    ])
    ->addText('repair_anchore', [
        'label' => 'Текст в якорной ссылке'
    ])
    ->addRepeater('repair_list', [
        'label' => 'Список этапов',
        'min' => 0,
        'max' => 4,
        'button_label' => 'Добавить этап',
        'layout' => 'row',
    ])
        ->addText('repair_list_title', [
            'label' => 'Название этапа'
        ]);

return $post;
