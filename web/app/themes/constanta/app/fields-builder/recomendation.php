<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("recomendation", ["title" => "Рекомендуем чинить протезы"]);

$post
    ->setLocation("page_template", "==", "views/template-repair.blade.php");

$post
    ->addText("recom_title", [
        "label" => "Заголовок блока"
    ])
    ->addText("recom_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addImage("recom_image", [
        "label" => "Изображение"
    ])
    ->addRepeater("recom_list", [
        "label" => "Список рекомендаций",
        "max" => 4,
        "button_label" => "Добавить рекомендацию",
        "layout" => "row",
    ])
        ->addText("recom_list_title", [
            "label" => "Название рекомендации"
        ]);

return $post;
