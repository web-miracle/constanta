<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("Course-list", ["title" => "Курсы"]);

$post
    ->setLocation("page_template", "==", "views/template-tech.blade.php");

$post
    ->addRelationship("course_list", [
        "label" => "Список курсов",
        "post_type" => ["cours"],
        "taxonomy" => [],
        "filters" => ["search"],
        "elements" => ["featured_image"],
        "min" => 1,
        "return_format" => "object"
    ]);
    // ->addRepeater("course_list_repeater", [
    //     "label" => "Список курсов",
    //     "min" => 0,
    //     "button_label" => "Добавить курс",
    //     "layout" => "row",
    // ])
    //     ->addImage("course_avatar", [
    //         "label" => "Фото курса",
    //         "preview_size" => "medium"
    //     ])
    //     ->addText("course_title", [
    //         "label" => "Заголовок курса"
    //     ])
    //     ->addWysiwyg("course_info", [
    //         "label" => "Описание курса"
    //     ])
    //     ->addText("course_duration", [
    //         "label" => "Длительность курса"
    //     ])
    //     ->addRepeater("course_price_repeater", [
    //         "label" => "Стоимость курса",
    //         "button_label" => "Добавить цену",
    //         "layout" => "table"
    //     ])
    //         ->addText("course_price", [
    //             "label" => "Цена курса"
    //         ])
    //         ->addTrueFalse("add_comment", [
    //             "label" => "Добавить комментарий"
    //         ])
    //         ->addText("course_price_comment", [
    //             "label" => "Пометка к цене"
    //         ])
    //             ->conditional("add_comment", "==", "1");

return $post;
