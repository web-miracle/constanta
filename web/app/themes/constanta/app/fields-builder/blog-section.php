<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('blog_section', ["title" => "Секция предосмотра постов"]);

$post
    ->setLocation('options_page', '==', 'company_setting');

$post
    ->addText("blog_section_title", [
        "label" => "Заголовок раздела"
    ])
    ->addText("blog_section_anchore", [
        "label" => "Текст в якорной ссылке"
    ])
    ->addTaxonomy("blog_section_taxonomy", [
        "label" => "Теги постов для вывода",
        "taxonomy" => "post_tag",
        "return_format" => "object"
    ]);

return $post;
