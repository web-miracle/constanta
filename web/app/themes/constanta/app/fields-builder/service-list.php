<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder("Services", ["title" => "Услуги"]);

$post
    ->setLocation("page_type", "==", "front_page");

$post
    ->addRepeater("services_list", [
            "label" => "Список услуг",
            "min" => 0,
            "button_label" => "Добавить услугу",
            "layout" => "block",
    ])
        ->addText("service_title", [
                "label" => "Название услуги",
                "requered" => 1
        ])
        ->addWysiwyg("service_editor", [
            "label" => "Описание услуги",
            "requered" => 1
        ]);

return $post;
