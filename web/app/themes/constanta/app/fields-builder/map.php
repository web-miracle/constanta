<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('yandex-map', ["title" => "Настройка yandex карты"]);

$post
    ->setLocation('options_page', '==', 'company_setting');
  
$post
    ->addField('map_point', 'yandex-map', [
		'label' => 'Выберите точку на карте'
	]);

return $post;
