<svg viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="13.5" cy="13.5" r="12.75" stroke="#0FBF9C" stroke-width="1.5"/>
<path d="M14.9387 14.2646C17.1781 14.2646 19 12.4107 19 10.1319C19 7.85352 17.1781 6 14.9387 6H11.0161V15.6282H10V16.651H11.0161V21H12.0672V16.651H15.6395V15.6282H12.0672V14.2646H14.9387ZM12.0672 7.02273H14.9388C16.5986 7.02273 17.9489 8.41745 17.9489 10.1319C17.9489 11.8467 16.5986 13.2419 14.9388 13.2419H12.0672V7.02273Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="14.5" y1="6" x2="14.5" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#0FBF9C"/>
<stop offset="1" stop-color="#38ACBD"/>
</linearGradient>
</defs>
</svg>
