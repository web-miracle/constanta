<svg viewBox="0 0 190 190" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="95" cy="95" r="92.5" stroke="url(#paint0_linear)" stroke-width="5"/>
<g filter="url(#filter0_d)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M162.636 28.2886C145.409 10.8247 121.469 0 95 0V5C120.312 5 143.182 15.449 159.535 32.2686L162.636 28.2886Z" fill="url(#paint1_linear)"/>
</g>
<defs>
<filter id="filter0_d" x="94" y="0" width="69.6357" height="35.2686" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="2"/>
<feGaussianBlur stdDeviation="0.5"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
<linearGradient id="paint0_linear" x1="-2.19231" y1="21.7141" x2="185.417" y2="24.8086" gradientUnits="userSpaceOnUse">
<stop stop-color="#0FBF9C"/>
<stop offset="1" stop-color="#38ACBD"/>
</linearGradient>
<linearGradient id="paint1_linear" x1="93.0044" y1="3.18753" x2="166.079" y2="15.6042" gradientUnits="userSpaceOnUse">
<stop stop-color="#F59D19"/>
<stop offset="0.670921" stop-color="#FF8179"/>
</linearGradient>
</defs>
</svg>
