<svg viewBox="0 0 190 190" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="95" cy="95" r="92.5" stroke="url(#paint0_linear)" stroke-width="5"/>
<g filter="url(#filter0_d)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M172.329 39.8032C155.097 15.7061 126.883 0 95 0V5C125.324 5 152.146 19.9973 168.452 42.9792L172.329 39.8032Z" fill="url(#paint1_linear)"/>
</g>
<defs>
<filter id="filter0_d" x="94" y="0" width="79.3286" height="45.9792" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="2"/>
<feGaussianBlur stdDeviation="0.5"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
<linearGradient id="paint0_linear" x1="-2.19231" y1="21.7141" x2="185.417" y2="24.8086" gradientUnits="userSpaceOnUse">
<stop stop-color="#0FBF9C"/>
<stop offset="1" stop-color="#38ACBD"/>
</linearGradient>
<linearGradient id="paint1_linear" x1="92.6925" y1="4.40801" x2="177.907" y2="16.5148" gradientUnits="userSpaceOnUse">
<stop stop-color="#F59D19"/>
<stop offset="0.670921" stop-color="#FF8179"/>
</linearGradient>
</defs>
</svg>
