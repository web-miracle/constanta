<svg viewBox="0 0 190 194" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="95" cy="99" r="92.5" stroke="url(#paint0_linear)" stroke-width="5"/>
<g filter="url(#filter0_d)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M147.322 19.6943C132.576 9.94629 114.953 4.20068 96 4.00513V9.00537C114.107 9.20239 130.932 14.7471 144.969 24.1345L147.322 19.6943Z" fill="url(#paint1_linear)"/>
</g>
<defs>
<filter id="filter0_d" x="92" y="0.00512695" width="59.322" height="28.1294" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.17 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
<linearGradient id="paint0_linear" x1="-2.19231" y1="25.7141" x2="185.417" y2="28.8086" gradientUnits="userSpaceOnUse">
<stop stop-color="#0FBF9C"/>
<stop offset="1" stop-color="#38ACBD"/>
</linearGradient>
<linearGradient id="paint1_linear" x1="94.4946" y1="5.86568" x2="148.612" y2="17.7191" gradientUnits="userSpaceOnUse">
<stop stop-color="#F59D19"/>
<stop offset="0.670921" stop-color="#FF8179"/>
</linearGradient>
</defs>
</svg>
