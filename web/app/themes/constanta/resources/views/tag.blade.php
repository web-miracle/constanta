@extends('layouts.app')

@section('content')
<div class="blog-page" data-tag="{{ $active_tag }}">
	@include("partials.home.sidebar",["mixin"=>"blog-page__sidebar"])
	<div class="blog-page__main">
		<p class="blog-page__title">
			{{ $title }} <sup>{{ $post_count }}</sup>
		</p>
		<p class="blog-page__description">
			{{ $desciption }}
		</p>

		<div class="blog-page__reports">		
			@foreach ($posts as $post)
			@include('partials.blog.card', ['mixin' => 'blog-item_shadow blog-page__item', 'post' => $post])
			@endforeach
		</div>
		
		<div style="text-align: center; width: 100%;">
			<button class="btn btn--transparent btn--black" data-post-add>
				Добавить постов
			</button>
		</div>
	</div>
</div>
@endsection
