<div class="certificate-slider">
	<div class="certificate-slider__outline">

		@if (empty($sertificate))
			@foreach ([1,2,3,4,5] as $item)
				<div class="certificate-slider__item">
					<img src='@asset("images/sertificate/$item.jpg")' alt="certificate">
				</div>
			@endforeach
		@else
			@foreach ($sertificate as $image)
				<div class="certificate-slider__item">
					<img
						data-origin="{{ $image['url'] }}"
						src="{{ $image['sizes']['medium'] }}"
						alt="{{ $image['alt'] }}"
						title="{{ $image['title'] }}">
				</div>
			@endforeach
		@endif

	</div>
</div>
