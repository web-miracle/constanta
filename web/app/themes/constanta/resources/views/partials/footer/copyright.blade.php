<div class="copyright {{ $mixin ?? '' }}">
	<p class="copyright__text">
		© КонстантаЛаб. Все права защищены 2018г.
	</p>
	<div class="copyright__social">
		<a href="{{ $social['telegram'] }}" target="_blank">
			@include('svg.telegram')
		</a>
		<a href="{{ $social['vk'] }}" target="_blank">
			@include('svg.vk')
		</a>
		<a href="{{ $social['instagram'] }}" target="_blank">
			@include('svg.instagram')
		</a>
	</div>
</div>
