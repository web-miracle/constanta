<div class="footer-nav {{ $mixin ?? '' }}">
	@foreach (["Пользовательское соглашение", "Гарантии", "Возврат", "Доставка"] as $item)
		<a href="#" class="footer-nav__link">
			{{ $item }}
		</a>
	@endforeach
</div>
