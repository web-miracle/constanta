<div class="footer-info {{ $mixin }}">
	<div class="footer-info__item">
		<div class="footer-info__icon footer-info__icon_small">
			@include('svg.place')
		</div>
		<div class="footer-info__content">
			<p class="footer-info__text">
				{!! $company_info['address'] !!}
			</p>
		</div>
	</div>

	<div class="footer-info__item">
		<div class="footer-info__icon">
			@include('svg.call')
		</div>
		<div class="footer-info__content">
			<a href="tel:{{ $company_info['phone'] }}" class="footer-info__text">
				{{ $company_info['phone'] }}
			</a>
			<p class="footer-info__text">
				{{ $company_info['shedule'] }}
			</p>
		</div>
	</div>

	<div class="footer-info__item">
		<div class="footer-info__icon">
			@include('svg.mail')
		</div>
		<div class="footer-info__content">
			<p class="footer-info__text">
				Обратная связь:
			</p>
			<a
				href="mailto:{{ $company_info['email'] }}"
				class="footer-info__text">
				{{ $company_info['email'] }}
			</a>
		</div>
	</div> 
</div>
<noindex>
<a style="display:none;" href="http://dnrdev.ru">Разработка</a>
</noindex>