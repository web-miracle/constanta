<section class="course-list" data-anchor="Курсы">
	@foreach($course_list as $index=>$course)
		@include('partials.course.index')
		@include('partials.modal.form', [
			"title" => $course_modal[0]->post_title, 
			"modal_id" => "course-button_$index",
			'politics' => $course_modal[0]->post_content,
			'data' => $course->title
		])
	@endforeach
</section>