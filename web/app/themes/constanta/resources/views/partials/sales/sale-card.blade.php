<div class="sale-card {{ $mixin ?? "" }}">
	@include('svg.sale' . $sale["discount_size"])
	<div class="sale-card__content">
		<p class="sale-card__percent">
			{{ $sale["discount_size"] }}<span>%</span>
		</p>
		<p class="sale-card__text">
			{{ $sale["discount_terms"] }}
		</p>
	</div>
</div>
