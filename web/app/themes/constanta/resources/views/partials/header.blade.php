@php do_action('get_header') @endphp

<header class="header"
	@if (empty($first_screen['image']))
		style="background-image: url(@asset('images/bg_main.jpg'))"
	@else
		style="background-image: url({{ $first_screen['image']['url'] }})"
	@endif
>
	@include('partials.header.navbar', ['mixin' => 'header__navbar'])
	@include('partials.header.info', ['mixin' => 'header__info'])
	@include('partials.header.advantages', ['mixin' => 'header__advantages'])

	@include('partials.header.social', ['mixin' => 'header__social'])
	@include('partials.header.dots', ['mixin' => 'header__dots'])
</header>
