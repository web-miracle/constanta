<section class="main">
	<div class="main__text">
		<p class="main__title">Зуботехническая лаборатория</p>
		<p class="main__subtitle"><span>Срочное</span> изготовление изделий для стоматологий в Москве</p>
	</div>

	<div class="main__buttons">
		<button class="btn">Оформить быструю заявку</button>
		<button class="btn btn_transparent">Рассчитать заказ онлайн</button>
	</div>

	<div class="main-info">
		<div class="main-info__item">
			<img src="@asset('images/main_price.png')" alt="" class="main-info__img">
			<p class="main-info__text">
				Цены ниже, чем у конкурентов. Мы проверяли! :)
			</p>
		</div>

		<div class="main-info__item">
			<img src="@asset('images/main_time.png')" alt="" class="main-info__img">
			<p class="main-info__text">
				Возможно изготовление заказа в этот же день
			</p>
		</div>

		<div class="main-info__item">
			<img src="@asset('images/main_cert.png')" alt="" class="main-info__img">
			<p class="main-info__text">
				Сертифицированная лаборатория. Гарантия качества.
			</p>
		</div>
	</div>

	<div class="main-social">
		<a href="#" class="main-social__item">@include('svg.telegram')</a>
        <a href="#" class="main-social__item">@include('svg.vk')</a>
		<a href="#" class="main-social__item">@include('svg.instagram')</a>
	</div>

	<div class="main-line">
		<div class="main-line__dot"></div>
		<div class="main-line__dot"></div>
		<div class="main-line__dot"></div>
		<div class="main-line__dot"></div>
		<div class="main-line__dot"></div>
	</div>
</section>