<div class="navbar {{ $mixin ?? '' }}">
	<div class="navbar__left">
		<a href="/">
			@if (empty($company_info['logo']))
				<img
					class="navbar__logo logo"
					src="@asset('images/logo.png')"
					alt="LOGO"
					title="Константа" />
			@else
				<img
					class="navbar__logo logo"
					src="{{ $company_info['logo']['url'] }}"
					alt="{{ $company_info['logo']['alt'] ?? 'LOGO' }}"
					title="{{ $company_info['logo']['title'] ?? $siteName }}" />
			@endif
		</a>
		<p class="navbar__address">
			{!! $company_info['address'] !!}
		</p>
	</div>
	<div class="navbar__right">
		<div class="navbar__company-info company-info">
			<a href="tel:{{ $company_info['phone'] }}" class="company-info__tel">
				{{ $company_info['phone'] }}
			</a>
			<p class="company-info__shedule">
				{{ $company_info['shedule'] }}
			</p>
		</div>
		@include('partials.header.navbar.menu', ['mixin' => 'navbar__menu'])
	</div>

	<div class="navbar__mobile">
		<div class="navbar__first">
			<a href="/">
				@if (empty($company_info['logo']))
					<img
						class="navbar__logo logo"
						src="@asset('images/logo.png')"
						alt="LOGO"
						title="Константа" />
				@else
					<img
						class="navbar__logo logo"
						src="{{ $company_info['logo']['url'] }}"
						alt="{{ $company_info['logo']['alt'] ?? 'LOGO' }}"
						title="{{ $company_info['logo']['title'] ?? $siteName }}" />
				@endif
			</a>
			@include('partials.header.navbar.menu', ['mixin' => 'navbar__menu'])
		</div>
		<div class="navbar__second">
			<div class="navbar__company-info company-info">
				<p class="navbar__address">
					{!! $company_info['address'] !!}
				</p>
				<div>
					<a href="tel:{{ $company_info['phone'] }}" class="company-info__tel">
						{{ $company_info['phone'] }}
					</a>
					<p class="company-info__shedule">
						{{ $company_info['shedule'] }}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
