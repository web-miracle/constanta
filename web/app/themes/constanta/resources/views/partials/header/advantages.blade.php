<div class="advantages {{ $mixin ?? '' }}">
	@foreach ($first_screen['company_pros'] as $pros)
		@if (empty($pros['image']) && empty($pros['title']))
		@else
			<div class="advantages__item">
				<img
					@if (empty($pros['image']))
						src="@asset('images/main_price.png')"
					@else
						src="{{ $pros['image']['url'] }}"
						alt="{{ $pros['image']['alt'] }}"
						title="{{ $pros['image']['title'] }}"
					@endif
					class="advantages__img"
				>
				<p class="advantages__text">
					{!! $pros["title"] !!}
				</p>
			</div>
		@endif
	@endforeach
</div>
