<div class="social {{ $mixin ?? '' }}">
	<a href="{{ $social['telegram'] }}" target="_blank" class="social__item">
		@include('svg.telegram')
	</a>
	<a href="{{ $social['vk'] }}" target="_blank" class="social__item">
		@include('svg.vk')
	</a>
	<a href="{{ $social['instagram'] }}" target="_blank" class="social__item">
		@include('svg.instagram')
	</a>
</div>
