<div class="menu {{ $mixin ?? '' }}">
	<button class="menu__gamburger">
		<span></span>
	</button>

	<div class="menu__drop">
		{{-- @if (has_nav_menu('primary_navigation')) --}}
			{!! wp_nav_menu([
					'theme_location' => 'primary_navigation',
					'container' => false,
					'menu_class' => 'menu__list',
					'depth' => 1
				]) !!}
		{{-- @else
			{{-- TODO: временно пока нет меню выводить кастомную из макета --}}
			{{-- <ul class="menu__list">
				@foreach (["Изготовление изделий для стоматологий", "Курсы зубных техников", "Изготовление протезов", "Ремонт протезов", "Блог"] as $element)
					<li>
						<a href="#">
							{{ $element }}
						</a>
					</li>
				@endforeach
			</ul>			 --}}
		{{-- @endif --}}
	</div>
</div>