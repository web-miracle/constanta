<div class="info {{ $mixin ?? '' }}">
	<h1 class="info__title">
		{{ $first_screen["header_title"] }}
	</h1>
	<h2 class="info__subtitle">
		{!! $first_screen["header_subtitle"] !!}
	</h2>

	<div class="info__buttons">
		@foreach ($first_screen['buttons'] as $index=>$button)
			<button 
				@if($button['modal'])
					data-modal="header-button_{{$index}}" 
				@else
					data-hash="courses"
				@endif
				class="btn {{ $button['layout'] }}"
			>
				{{ $button["text"] }}
			</button>
			@if($button['modal'])
				@include('partials.modal.form', [
					"title" => $button['text'], 
					"modal_id" => "header-button_$index",
					'politics' => $button['modal'][0]->post_content
				])
			@endif
		@endforeach
	</div>
</div>
