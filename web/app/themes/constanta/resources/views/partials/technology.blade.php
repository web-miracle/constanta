<section class="technology" data-anchor="{{ $technology['anchore'] }}">
	<p class="technology__title">
		{{ $technology["title"] }}
	</p>
	<div class="technology__content">
		@foreach ($technology['list'] as $item)
			@include(
				'partials.technology-card',
				[
					'mixin' => 'technology__technology-card',
					'technology' => $item
				]
			)
		@endforeach
	</div>

	@php
		$iteration = ceil(count($technology['list']) / 4);
	@endphp
	@while ($iteration > 0)
		<svg class="technology__svg-line" viewBox="0 0 1360 110">
			<path />
		</svg>
		@php
			$iteration--;
		@endphp
	@endwhile
</section>