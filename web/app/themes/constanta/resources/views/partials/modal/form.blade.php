<div class="modal-form__wrap" data-modal="{{ $modal_id }}">
	<div class="modal-form">
		<button class="modal-form__close modal__close">
			@include('svg.close')
		</button>
		<form class="modal-form__content">
			<p class="modal-form__title jFormTitle">{{ $title }}</p>

			<input type="text" required name="FIO" class="modal-form__input form__input jName" placeholder="Ваше имя*">
			<input type="tel" pattern="[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}" title="Нужный формат +99(99)9999-9999" required name="phone" class="modal-form__input form__input jTel" placeholder="Телефон*">
			<input type="hidden" name="data" class="jData" value="{{ $data ?? '' }}" />

			<button class="modal-form__btn btn btn--gradient btn--black">Отправить</button>

			<p class="modal-form__policy">
				Нажимая на кнопку "Отправить", вы соглашаетесь на обработку персональных данных в соответствии с <button data-modal="politics_{{$modal_id}}">политикой в отношении обработки персональных данных</button>
			</p>
		</form>
	</div>
</div>

@include('partials.modal.text', ['content' => $politics, 'modal_id' => "politics_$modal_id"])