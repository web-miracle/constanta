<section class="recomend" data-anchor="{{ $recomendation['anchore'] }}">
	<p class="recomend__title">
		{{ $recomendation['title'] }}
	</p>

	<div class="recomend__content">
		<div>
			@foreach ($recomendation['list'] as $key => $item)
				@if ($key % 2 === 1)
					<div class="recomend__item">
						{{ $item['recom_list_title'] }}
					</div>
				@endif
			@endforeach
			{{-- @for ($i = 0; $i < 2; $i++)
			@endfor --}}
		</div>
		
		<div class="recomend__img-wrap">
			<div class="recomend__img">
				@if (empty($recomendation['image']))
					<img src="@asset('images/repair_img.jpg')">
				@else
					<img
						src="{{ $recomendation['image']['url'] }}"
						alt="{{ $recomendation['image']['alt'] }}"
						title="{{ $recomendation['image']['title'] }}">
				@endif
			</div>
		</div>

		<div>
			@foreach ($recomendation['list'] as $key => $item)
				@if ($key % 2 === 0)
					<div class="recomend__item">
						{{ $item['recom_list_title'] }}
					</div>
				@endif
			@endforeach
		</div>
	</div>
</section>