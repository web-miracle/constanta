<section class="course-admin">
	<p class="course-admin__title">Курсы проводит</p>
	<div class="course-admin__img-wrap">
		<div class="course-admin__img">
			@if (empty($lektor['lektor_avatar']))
				<img
					src="@asset('images/course-admin.png')"
					alt="admin"
					title="{{ $lektor['lektor_fio'] }}">
			@else
				<img
					src="{{ $lektor['lektor_avatar']['url'] }}"
					alt="{{ $lektor['lektor_avatar']['alt'] }}"
					title="{{ $lektor['lektor_avatar']['title'] }}">
			@endif
		</div>
	</div>
	<div style="text-align: center">
		<p class="course-admin__name">
			{!! $lektor['lektor_fio'] !!}
		</p>
		<!-- <ul class="course-admin__list">
			<li>Техник высшей категории</li>
			<li>Опыт работы: 23 года</li>
		</ul> -->
		{!! $lektor['lektor_info'] !!}
	</div>
</section>