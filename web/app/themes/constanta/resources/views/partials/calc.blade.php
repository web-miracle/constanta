<section class="calc-section" data-anchor="Калькулятор цен">
	<div class="calc-section__title">
		Рассчитайте ваш заказ онлайн
	</div>

	<div class="calc-section__content calc">
		<div class="calc__sidebar categories jCategories">
			@foreach ($categories as $index=>$item)
			<div class="categories__item" data-cat="{{ $index }}">
				{{ $item['nom_title'] }}
			</div>
			@endforeach
		</div>
		<div class="calc__content">
			<div class="calc__list">
			<table class="table jTable">
				<thead>
					<tr class="table__row">
						<th class="table__headercell">Код</th>
						<th class="table__headercell">Наименование</th>
						<th class="table__headercell">Сроки</th>
						<th class="table__headercell">Цена</th>
						<th class="table__headercell">Количество</th>
						<th class="table__headercell">Сумма</th>
					</tr>
				</thead>
				@foreach ($categories as $index=>$item)
					<tbody class="table__tbody jTbody" id="tbody_{{ $index }}">
					@foreach($item['nom_products'] as $index2=>$product)
						<tr class="table__row jRow" data-id="{{ $index2 }}">
							<td class="table__cell jCode">{{ $product['code'] }}</td>
							<td class="table__cell jName">{{ $product['name'] }}</td>
							<td class="table__cell jDuration">{{ $product['duration'] }}</td>
							<td class="table__cell"><span class="jPrice">{{ $product['price'] }}</span> ₽</td>
							<td class="table__cell table__count">
								<span class="jMinus table__minus table__minus_disabled">-</span>
								<span class="jCount">0</span>
								<span class="jPlus table__plus">+</span>
							</td>
							<td class="table__cell"><span class="jSumm">0</span> ₽</td>
						</tr>
					@endforeach
					</tbody>
				@endforeach

			</table>
			</div>
			<div class="calc__summ summ">
				<div class="summ__discount">
					<div class="summ__total"><span>Итого:</span><span class="summ__final jTotal">255 132</span>₽</div>
					@foreach ($discount as $sale)
					<div class="summ__skidka">
						-<span class="jDiscount">{{ $sale['discount_size'] }}</span>% <span>{{ $sale['discount_terms'] }}</span>
						<span class="summ__final jDiscTotal">0</span>₽
					</div>
					@endforeach
				</div>
				<div class="summ__content">
					<button data-modal="calc" class="jSubmit summ__submit btn btn--black btn--gradient">Оформить заказ</button>
					<div class="summ__details">
						Нажимая на кнопку "Оформить заказ", вы соглашаетесь на обработку персональных данных в соответствии с <a class="summ__politics" href="#">политикой в отношении обработки персональных данных</a>
					</div>

					@include('partials.modal.form', ['title' => 'Сделать заказ', 'modal_id' => 'calc', 'politics' => ''])
					<div id="jData" style="display: none;"></div>
				</div>
			</div>
		</div>
	</div>
</section>