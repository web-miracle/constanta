@php do_action('get_footer') @endphp

<footer class="footer">
  <div class="footer__left">
    @include('partials.footer.footer-nav', ['mixin' => 'footer__footer-nav'])
    @include('partials.footer.footer-info', ['mixin' => 'footer__footer-info'])
    @include('partials.footer.copyright', ['mixin' => 'footer__copyright'])
  </div>

  <div class="footer__right">
    @include('partials.footer.sertificate-slider', ['mixin' => 'footer__sertificate-slider'])
  </div>

</footer>

@php wp_footer() @endphp
