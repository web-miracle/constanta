<section class="about-us" data-anchor="{{ $about_us['anchore'] }}">
	<div class="about-us__content">
		<p class="about-us__title">
			{{ $about_us["title"] }}
		</p>
		<p class="about-us__desc">
			{{ $about_us["info"] }}
		</p>
		
		<div class="about-us__dots"></div>

		@if (!empty($services))
			<ul class="about-us__service-list service-list">
				@foreach ($services as $index=>$service)
					<li class="service-list__item">
						<button href="#" data-modal="service_{{ $index }}" class="service-list__link">
							{{ $service['service_title'] }}
						</button>
					</li>
					@include('partials.modal.text', ['content' => $service['service_editor'], 'modal_id' => "service_$index"])
				@endforeach
			</ul>
		@endif
	</div>

	<i class="about-us__circle about-us__circle--left"></i>
	<i class="about-us__circle about-us__circle--right"></i>
</section>