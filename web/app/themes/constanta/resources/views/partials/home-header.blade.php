@php do_action('get_header') @endphp

<div
	class="header header--no-full"
	style="background-image: url(@asset('images/bg_sales.jpg'))">
	@include('partials.header.navbar', ['mixin' => 'header__navbar'])
</div>
