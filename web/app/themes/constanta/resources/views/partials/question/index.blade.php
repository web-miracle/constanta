<section class="question" data-anchor="Остались вопросы?">
	<div class="question__form">
		<p class="question__title">
			Остались вопросы?
		</p>
		<p class="question__desc">
			Оставьте свои контактные данные и наш менеджер перезвонит вам в течение <strong>15 минут.</strong>
		</p>
		@include('partials.question.form')
	</div>

	<div class="question__map">
		<div id="map"></div>
	</div>
</section>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
</script>
<script type="text/javascript">
	(function(){
		window.addEventListener('load', function() {
			var script = document.createElement('script')
			script.setAttribute('src', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU');

			document.body.appendChild(script);

			script.addEventListener('load', function() {
			    ymaps.ready(init);
			  
			    function init(){ 
			        var myMap = new ymaps.Map("map", {
			            center: [{{ $map_point->marks[0]->coords[0] }}, {{ $map_point->marks[0]->coords[1] }}],
			            zoom: {{ $map_point->zoom }},
			            controls: []
			        }); 
			        
			        var myPlacemark = new ymaps.Placemark([{{ $map_point->marks[0]->coords[0] }}, {{ $map_point->marks[0]->coords[1] }}], {
			            hintContent: '{{ $map_point->marks[0]->content }}',
			        }, {
			        	iconLayout: 'default#image',
			        	iconImageHref: "@asset('images/marker.svg')",
			        	iconImageSize: [29, 44],
			        	iconImageOffset: [-14.5, -22]
			        });
			        
			        myMap.geoObjects.add(myPlacemark);
			    }
			});
		});
	})();
</script>