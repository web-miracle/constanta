<form class="form">
	<input type="text" class="form__input jName" placeholder="Ваше имя*">
	<input type="tel" class="form__input jTel" placeholder="+7">
	<button class="form__btn btn btn--gradient btn--black">Отправить</button>
	<p class="form__policy">
		Нажимая на кнопку "Отправить", вы соглашаетесь на обработку персональных данных в соответствии с политикой в отношении <a href="#">обработки персональных данных</a>
	</p>
</form>