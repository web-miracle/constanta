<section class="process" data-anchor="{{ $repair['anchore'] }}">
	<p class="process__title">
		{{ $repair['title'] }}
	</p>
	
	@foreach ($repair['list'] as $item)		
		<div class="process__item">
			{{ $item['repair_list_title'] }}
		</div>
	@endforeach

</section>