<section class="blog-section" data-anchor="{{ $blog_section['anchore'] }}">
	<p class="blog-section__title">
		{{ $blog_section['title'] }}
	</p>
	
	@include('partials.blog.pagination')		

	<div class="blog-section__content">
		@foreach ($blog_section['posts'] as $post)
			@include('partials.blog.card', ['mixin'=> 'blog-section__blog-item', 'post' => $post])
		@endforeach
	</div>

	<a href="{{ $blog_link ?? '#' }}" class="btn btn--transparent blog-section__more">
		Читать другие статьи >
	</a>

	<svg class="blog-section__bg" viewBox="0 0 1360 838">
		<path />
	</svg>
</section>