<div class="blog-item blog-item_active {{ $mixin ?? "" }}">
	<div class="blog-item__img">
		<img
			src="{{ get_the_post_thumbnail_url($post, 'large') ? get_the_post_thumbnail_url($post, 'large') : ($post->img ?? 'https://via.placeholder.com/350x150') }}"
		>
	</div>

	<div class="blog-item__content">
		<a href="{{ get_permalink($post) }}" class="blog-item__title">
			{{ $post->post_title }}
		</a>
		
		<p class="blog-item__desc">
			{{ $post->post_excerpt }}
		</p>

		<div class="blog-item__row">
			@php
				$primary_tag = empty(wp_get_post_tags($post->ID)) ? null : wp_get_post_tags($post->ID)[0];
			@endphp
			@if ($primary_tag)
				<a href="{{ get_tag_link($primary_tag->term_id) }}" class="btn btn--small btn--gradient">
					<span data-text="{{ get_field("short_tag_name", $primary_tag) ?? "tag" }}"></span>
					{{ get_field("short_tag_name", $primary_tag) ?? "tag" }}
				</a>
			@else
				<div></div>
			@endif
			<p class="blog-item__date">
				{{ date_format(date_create($post->post_date), "d.m.Y") }}
			</p>
		</div>
	</div>
</div>
