<section class="how-works" data-anchor="{{ $step_work_section['anchore'] }}">
	<p class="how-works__title">
		{{ $step_work_section["title"] }}
	</p>

	<div class="how-works__content">
		@foreach ($step_work as $index => $step)
			@include(
				'partials.how_works.card',
				[
					'mixin' => 'how-works__works-card',
					'stage' => $index,
					'step' => $step
				]
			)
		@endforeach
	</div>
</section>