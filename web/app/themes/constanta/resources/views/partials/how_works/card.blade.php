<div class="works-card {{ $mixin ?? '' }}">
	<div class="works-card__header">
		<p class="works-card__title">
			{{ $step['how_works_title'] ?? '' }}
		</p>
	</div>

	<p class="works-card__desc">
		{!! $step['how_works_area'] ?? '' !!}
	</p>
</div>
