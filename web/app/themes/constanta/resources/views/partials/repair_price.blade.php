<section class="price" data-anchor="Сколько стоит ремонт?">
	<p class="price__title">{{ $price['title'] }}</p>

	<!-- <p class="price__list-title">Наша лаборатория проводит следующие виды ремонта:</p>
	<ul class="price__list">
		<li>перелом протеза</li>
		<li>частичная приварка зуба</li>
	</ul> -->
	<p class="price__list-title">
		{!! $price['info'] !!}
	</p>

	@foreach($price['list'] as $item)
		<div class="price__row">
			@foreach($item as $card)
				<div class="option">
					<p class="option__text">
						{{ $card['price_list_title'] }}
					</p>
					<p class="option__price">
						{{ $card['price_list_price'] }}
					</p>
				</div>
			@endforeach
		</div>
	@endforeach

	<div class="price__btn">
		<button data-modal="repair_modal" class="btn btn--gradient btn--black">
			Хочу отремонтировать протез!
		</button>
		@include('partials.modal.form', ['modal_id' => 'repair_modal', 'title' => $repair_modal[0]->post_title, 'politics' => $repair_modal[0]->post_content])
	</div>
</section>

