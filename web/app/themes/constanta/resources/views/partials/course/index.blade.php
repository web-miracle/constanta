<div class="course course-list__course"
	id="courses"
	data-course>
	<div class="course__text">
		<p class="course__title">
			{{ $course->title }}
		</p>
		
		<ul class="course__list">
		@if($course->composition)
			@foreach ($course->composition as $step)
				<li class="course__item">
					{{ $step['stage'] }}
				</li>
			@endforeach
		@endif
		@if($course->info)
			{!! $course->info !!}
		@endif
		</ul>
		
		<div class="course__row">
			<button class="course__btn btn btn--gradient btn--black" data-modal="course-button_{{$index}}">
				@if($course->composition)
					Записаться
				@elseif ($course->info)
					Получить консультацию
				@endif
			</button>
			

			<div class="course__icon">
				@include('svg.course_time')
				<div>
					<p>
						{{ $course->duration }}
					</p>
				</div>
			</div>
			<div class="course__icon">
				@include('svg.course_price')
				<div>
					@foreach($course->prices as $price)
						<p>
							{{ $price['price'] }}
							@if($price['add_comment'])
								<span>
									{{ $price['comment'] ?? "" }}
								</span>
							@endif
						</p>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<div class="course__img">
		@if (empty($course->image))
			<img src="@asset('images/course_img.jpg')">
		@else
			<img src="{{ $course->image }}">
		@endif
	</div>
</div>
{{--<div class="course course_revers course-list__course course-list__course_revers">
	<p class="course__title">
		{{ $course->title }}
	</p>

	@if ($course->composition)
		<ul class="course__list">
			@foreach ($course->composition as $step)
				<li class="course__item">
					{{ $step['stage'] }}
				</li>
			@endforeach
		</ul>
	@endif

	@if ($course->info)
		<div class="course__info">
			{!! $course->info !!}
		</div>
	@endif


	<div class="course__row">
		<button 
			class="course__btn btn btn--gradient
			@if (is_page_template("views/template-make.blade.php"))
				course__btn_protez
			@endif
			btn--black"
			data-modal="course_button_{{$index}}"
		>
			@if (is_page_template("views/template-tech.blade.php"))
			Записаться
			@else
			Получить консультацию
			@endif
		</button>

		@include('partials.modal.form',['modal_id' => "course_button_$index", "title" => $course_modal[0]->post_title."\n\r'".$course->title."'", "politics" => $course_modal[0]->post_content])
		
		<div class="course__icon">
			@include('svg.course_time')
			<div>
				<p>
					{{ $course->duration }}
				</p>
			</div>
		</div>
		<div class="course__icon">
			@include('svg.course_price')
			<div>
				@foreach($course->prices as $price)
					<p>
						{{ $price['price'] }}
						@if($price['add_comment'])
							<span>
								{{ $price['comment'] ?? "" }}
							</span>
						@endif
					</p>
				@endforeach
			</div>
		</div>
	</div>

	<div class="course__img">
		@if (empty($course->image))
			<img src="@asset('images/course_img.jpg')">
		@else
			<img src="{{ $course->image }}">
		@endif
	</div>
</div>
--}}