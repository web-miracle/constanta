<div class="{{ $mixin ?? "" }} sidebar">
	<button class="sidebar__close">
		@include('svg.close')
	</button>

	<p class="sidebar__title">
		Теги
	</p>

	<ul class="sidebar__tag-list">
		@foreach ($tags as $tag)
			<li>
				<a
					href="{{
						urldecode($tag->slug) === $active_tag ?
							$blog_link :
							get_tag_link($tag->term_id)
						}}"
					class="sidebar__tag-item
						{{
							(urldecode($tag->slug) === $active_tag) ?
								'sidebar__tag-item--active' :
								''
						}}"
				>
					{{ $tag->name }}
					<span class="sidebar__tag-deactive">&times</span>
				</a>
			</li>
		@endforeach
	</ul>
</div>

<div class="sidebar__btn-wrap">
	<button class="sidebar__btn">➔</button>
</div>