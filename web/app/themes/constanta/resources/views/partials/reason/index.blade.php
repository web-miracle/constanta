<section class="reason" data-anchor="Почему ломается?">
	<div class="reason__row">
		@include('svg.question')
		<div class="reason__text">
			<p class="reason__title">
				{{ $reason['title'] }}
			</p>
			<p class="reason__desc">
				{{ $reason['subtitle'] }}
			</p>
		</div>
	</div>

	@include('partials.reason.list')
</section>