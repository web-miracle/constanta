<section class="sales" data-anchor="Скидки">
	<div class="sales__text">
		<p class="sales__title">
			Чем больше<br>
			заказываете - тем<br>
			больше скидка!
		</p>
		<p class="sales__desc">
			Получайте скидки за объем<br>
			совершенных заказов.
		</p>
	</div>

	<div class="sales__content">
		@foreach ($discount as $sale)
			@include('partials.sales.sale-card', ['mixin' => 'sales__sale-card', 'sale' => $sale])
		@endforeach		
	</div>
</section>