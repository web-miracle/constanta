<div class="examples-gallery {{ $mixin ?? '' }}">
	<div class="examples-gallery__navs">
		<button class="examples-gallery__nav-link examples-gallery__nav-link--left examples-gallery__nav-link--disabled" data-direction="prev">
			@include('svg.examples_arrow')
		</button>
		<div class="examples-gallery__nav-info">
			1 из 2
		</div>
		<button class="examples-gallery__nav-link" data-direction="next">
			@include('svg.examples_arrow')
		</button>
	</div>

	<div class="examples-gallery__content">
		@if (empty($works))
			@foreach ([1,2,3,4,5,6,7,8,9] as $element)
				<img src="@asset('images/examples/1.jpg')">
				<img src="@asset('images/examples/2.jpg')">
				<img src="@asset('images/examples/5.jpg')">
				<img src="@asset('images/examples/6.jpg')">
				<img src="@asset('images/examples/7.jpg')">
				<img src="@asset('images/examples/1.jpg')">
				<img src="@asset('images/examples/2.jpg')">
				<img src="@asset('images/examples/3.jpg')">
				<img src="@asset('images/examples/4.jpg')">
			@endforeach
		@else
			@foreach ($works as $element)
				<img
					data-origin="{{ $element['about_image']['url'] }}"
					src="{{ $element['about_image']['sizes']['medium'] }}"
					alt="{{ $element['about_image']['alt'] }}"
					title="{{ $element['about_image']['title'] }}"
				>
			@endforeach
		@endif
	</div>
</div>