<section class="examples" data-anchor="{{ $works_section['anchore'] }}">
	<p class="examples__title">
		{{ $works_section["title"] }}
	</p>
	@include('partials.examples.gallery', ['mixin' => 'examples__examples-gallery'])
</section>
