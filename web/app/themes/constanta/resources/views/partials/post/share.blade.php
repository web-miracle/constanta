<div class="post-share">
	<p class="post-share__title">Поделиться:</p>

	<div class="post-share__content">
		<a href="{{ $shared->vk }}" target="_blank" class="post-share__link">
			<img src="@asset('images/ic_vk.png')" alt="vk">
		</a>
		<a href="{{ $shared->facebook }}" target="_blank" class="post-share__link">
			<img src="@asset('images/ic_facebook.png')" alt="vk">
		</a>
		<a href="{{ $shared->telegram }}" target="_blank" class="post-share__link">
			<img src="@asset('images/ic_telegram.png')" alt="telegram">
		</a>
		<a href="{{ $shared->twitter }}" target="_blank" class="post-share__link">
			<img src="@asset('images/ic_twitter.png')" alt="twitter">
		</a>
		<a href="{{ $shared->google }}" target="_blank" class="post-share__link">
			<img src="@asset('images/ic_google.png')" alt="google">
		</a>
		<a href="{{ $shared->livejournal }}" target="_blank" class="post-share__link">
			<img src="@asset('images/ic_live.png')" alt="live">
		</a>

	</div>
</div>