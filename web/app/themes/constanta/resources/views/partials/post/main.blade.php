<div class="post">
	<img src="{{ get_the_post_thumbnail_url($post, 'large') ? get_the_post_thumbnail_url($post, 'large') : ($post->img ?? 'https://via.placeholder.com/350x150') }}">

	{!! $post->post_content !!}
</div>