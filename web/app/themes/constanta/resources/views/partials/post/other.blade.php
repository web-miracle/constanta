<a href="{{ get_permalink($post) }}" class="sidebar-item">
	<img
		src="{{ get_the_post_thumbnail_url($post, 'large') ? get_the_post_thumbnail_url($post, 'large') : ($post->img ?? 'https://via.placeholder.com/350x150') }}"
		class="sidebar-item__img"
	>

	<div class="sidebar-item__text">
		<p class="sidebar-item__title">
			{{ $post->post_title }}
		</p>

		<p class="sidebar-item__date">
			{{ date_format(date_create($post->post_date), "d.m.Y") }}
		</p>
	</div>
</a>