<div class="post-sidebar">
	<p class="post-sidebar__title">Другие статьи</p>

	<div class="post-sidebar__content">
		@foreach ($other_posts as $post)
			@include('partials.post.other', ["post" => $post])
		@endforeach
	</div>
</div>