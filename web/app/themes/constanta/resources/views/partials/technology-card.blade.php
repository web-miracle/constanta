<div class="technology-card {{ $mixin }}">
	<p class="technology-card__title">
		{!! $technology['tech_title'] ?? '' !!}
	</p>
	<div class="technology-card__text">
		{!! $technology['tech_excerpt'] ?? '' !!}
	</div>
	<a href="{{ $technology['link'] ?? '' }}" class="technology-card__link">
		Подробнее
	</a>
</div>
