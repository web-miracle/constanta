{{--
  Template Name: изготовление протезов
--}}

@extends('layouts.app')

@section('content')
	@include('partials.about_us')
  	@include('partials.course_list')
	@include('partials.examples.index')
	@include('partials.how_works.index')
	@include('partials.blog.index')
	@include('partials.question.index')
@endsection