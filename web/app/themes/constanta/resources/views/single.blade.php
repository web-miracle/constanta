@extends('layouts.app')

@section('content')
<section class="post-page">
	<a href="{{ $blog_link }}" class="post-page__back">
		Назад в блог
	</a>

	<p class="post-page__date">
		{{ date_format(date_create($post->post_date), "d.m.Y") }}
	</p>
	<h1 class="post-page__title">
		{{ $post->post_title }}
	</h1>
	<div class="post-page__content">
		@include('partials.post.main')
		@include('partials.post.sidebar')
	</div>

	@include('partials.post.share')
</section> 	
@endsection