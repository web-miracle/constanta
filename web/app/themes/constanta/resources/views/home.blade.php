@extends('layouts.app')

@section('content')
<div class="blog-page" data-tag="">
	@include("partials.home.sidebar",["mixin"=>"blog-page__sidebar"])
	<div class="blog-page__main">
		<p class="blog-page__title">
			{{ $title }} <sup>{{ wp_count_posts("post")->publish }}</sup>
		</p>
		<p class="blog-page__description">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut posuere odio lectus, sit amet eleifend leo commodo sed. Nam mollis et lectus non suscipit. Proin nec tellus quis urna tincidunt faucibus. Phasellus malesuada arcu et erat semper, nec semper sem rutrum. Duis consectetur, diam tempus sodales posuere, augue dolor pretium risus, nec vestibulum nulla ex feugiat nisl. Phasellus a justo urna. 
		</p>

		<div class="blog-page__reports">
			@foreach ($posts as $post)
				@include('partials.blog.card', ['mixin' => 'blog-item_shadow blog-page__item', 'post' => $post])
			@endforeach
		</div>
		
		<div style="text-align: center; width: 100%;">
			<button class="btn btn--transparent btn--black" data-post-add>
				Добавить постов
			</button>
		</div>
	</div>
</div>
@endsection
