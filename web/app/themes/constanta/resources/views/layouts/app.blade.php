<!doctype html>
<html {!! get_language_attributes() !!}>

	@include('partials.head')

	<body @php body_class() @endphp>

		@if (is_home() || is_tag() || is_single())
			@include('partials.home-header')
		@else
			@include('partials.header')
		@endif

		@yield('content')

		@include('partials.footer')
		
	</body>
</html>
