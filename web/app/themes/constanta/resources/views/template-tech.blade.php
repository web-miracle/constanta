{{--
  Template Name: для зубных техников
--}}

@extends('layouts.app')

@section('content')
  	@include('partials.about_us')
  	@include('partials.course_list')
  	@include('partials.course_admin')
  	@include('partials.blog.index')
	@include('partials.question.index')
@endsection