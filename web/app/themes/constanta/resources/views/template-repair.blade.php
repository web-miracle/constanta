{{--
  Template Name: ремонт протезов
--}}

@extends('layouts.app')

@section('content')
  	@include('partials.recomendation')
  	@include('partials.reason.index')
  	@include('partials.process.index')
  	@include('partials.repair_price')
  	@include('partials.how_works.index')
	@include('partials.blog.index')
	@include('partials.question.index')
@endsection