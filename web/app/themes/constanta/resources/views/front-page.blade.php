@extends('layouts.app')

@section('content')
	@include('partials.about_us')
	@include('partials.technology')
	@include('partials.calc')
	@include('partials.sales')
	@include('partials.examples.index')
	@include('partials.how_works.index')
	@include('partials.blog.index')
	@include('partials.question.index')
@endsection
