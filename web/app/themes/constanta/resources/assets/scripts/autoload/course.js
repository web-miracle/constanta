class CourseAnim {
	constructor (node) {
		if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node 
    this.elemTop = this.elem.offsetTop
	}

	init () {
		if (document.body.clientWidth >= 768) {
      document.addEventListener('scroll', () => {
        const rem = parseFloat(getComputedStyle(document.documentElement).fontSize)
        const top = window.pageYOffset + window.innerHeight - 30 * rem
  
        this.elemTop = this.elem.offsetTop
  
        if (top > this.elemTop) {
          this.elem.classList.add('course_show')
          return
        }
        this.init()
      }, {once: true})
    }
    else {
      this.elem.classList.add('course_show')
    }
	}
}

window.addEventListener('load', () => {
  const course = document.querySelectorAll('[data-course]')

  course.forEach(item => {
    try {
      let itemInitObj = new CourseAnim(item)
      itemInitObj.init()
    } catch(e) {
      console.log(e)
    }
  })
})
