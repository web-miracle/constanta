class Button {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
  }

  init () {
    if (this.elem.querySelector('span[data-text]'))
      return

    const span = this.createHoverNode()
    this.elem.insertBefore(span, this.elem.firstChild)
  }

  createHoverNode () {
    const html = document.createElement('span')
    const text = this.elem.innerText

    html.setAttribute('data-text', text)

    return html
  }
}

window.addEventListener('load', () => {
  const buttons = Array.from(document.querySelectorAll('.btn'))

  buttons.forEach(button => {
    try {
      let buttonInitObj = new Button(button)
      buttonInitObj.init()
    } catch(e) {
      console.log(e)
    }
  })
})
