class Sidebar {
	constructor (node) {
		if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.parent = document.querySelector('.sidebar')
    this.close = this.parent.querySelector('.sidebar__close')
    this.content = document.querySelector('.blog-page__main')
	}

	init () {

		if (document.body.clientWidth < 768) {
      this.elem.addEventListener('click', () => {
        this.parent.style.display = 'block'
        this.elem.style.display = 'none'
        this.content.style.display = 'none'        
      })

      this.close.addEventListener('click', () => {
        this.parent.style.display = 'none'
        this.elem.style.display = 'block'
        this.content.style.display = 'block'
      })
    }
	}
}

window.addEventListener('load', () => {
  const sidebar = document.querySelector('.sidebar__btn-wrap')

 try {
    let sidebarInitObj = new Sidebar(sidebar)
    sidebarInitObj.init()
  } catch(e) {
    console.log(e)
  }
})
