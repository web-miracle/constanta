class Anchor {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.activeNow = null

    this.setLinks()
  }

  init () {
    document.addEventListener('scroll', () => {
      const rem = parseFloat(getComputedStyle(document.documentElement).fontSize)
      const top = window.pageYOffset + window.innerHeight - 25 * rem

      const iterations = this.linksTop.length
      let activateId = null

      for (let i = 0; i < iterations; i++) {
        if (top > this.linksTop[i]) {
          activateId = i
        }
      }

      this.activateLink(activateId)
    })
  }

  setLinks () {
    this.links = Array.from(document.querySelectorAll('[data-anchor]'))
    this.links = this.links.filter(link => link.style.display !== 'none')
    this.linksName = this.links.map(link => link.getAttribute('data-anchor'))
    this.linksTop = this.links.map(link => link.offsetTop)

    this.buildHTML()
  }

  buildHTML () {
    this.links.forEach((link, index) => {
      const dot = this.dotHTML(this.linksName[index], index)
      this.elem.insertBefore(dot, this.elem.lastElementChild)
    })

    this.dots = Array.from(this.elem.querySelectorAll('.dots__link'))
  }

  activateLink (id) {
    if (this.activeNow === id)
      return

    this.activeNow = id
    const link = document.querySelector(`[data-anchor-link="${id}"]`)
    const active = this.dots.map(link => {
      if (link.classList.contains('dots__link--active'))
        return link

      return null
    })

    active.forEach(link => {
      if (link)
        link.classList.remove('dots__link--active')
    })

    if (link)
      link.classList.add('dots__link--active')
  }

  dotHTML (name, id) {
    const html = document.createElement('div')
    html.classList.add('dots__link')
    html.setAttribute('data-anchor-link', id)

    const dot = document.createElement('div')
    dot.classList.add('dots__dot')

    const title = document.createElement('p')
    title.classList.add('dots__name')
    title.innerText = name

    html.appendChild(dot)
    html.appendChild(title)

    return html
  }
}

window.addEventListener('load', () => {
  if (document.body.clientWidth < 1200) 
    return
//   console.log(document.body.clientWidth)
  const nav = document.querySelector('.dots')

  try {
    let navInitObject = new Anchor(nav)
    navInitObject.init()
  } catch(e) {
    console.log(e)
  }
})
