class SertificateSlider {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.outline = this.elem.querySelector('.certificate-slider__outline')
    this.images = Array.from(this.outline.querySelectorAll('img'))
    this.pages = Math.ceil(this.images.length)

    this.buildModal()
  }

  init () {
    this.setPage()

    this.images.forEach((image, index) => {
      image.addEventListener('click', ({target}) => {
        const src = target.getAttribute('data-origin') || target.src
        this.activeModal(src, index)
      })
    })

    this.drag()
  }

  drag () {
    this.outline.addEventListener('mousedown', event => this.dragStart(event), {once: true})
  }

  dragStart (event) {
    console.log(123)
    this.drag_move = true
    this.move = event.x
    const eventHandler = this.dragMove.bind(this)

    document.addEventListener('mousemove', eventHandler)

    document.addEventListener('mouseup', () => {
      document.removeEventListener('mousemove', eventHandler)
      this.drag_move = false
      const page = this.images.reduce((result, image, index) => {
        if (image.offsetLeft < this.offset)
          result = index + 1

        return result
      }, 1)

      this.setPage(page)

      this.drag()
    }, {once: true})
  }

  dragMove (event) {
    this.outline.style.transition = ''
    const diff = this.move - event.x
    this.move = event.x
    let offset = this.offset + diff
    const last_img = this.images[this.images.length - 1].offsetLeft

    if (offset < 0)
      offset = 0
    else if (offset > last_img)
      offset = last_img

    if (this.drag_move)
      this.outline.style.transform = `translateX(-${offset}px)`

    this.offset = offset
    this.outline.style.transition = 'all 300ms ease-out'
  }

  buildModal () {
    const modal = document.createElement('div')
    modal.classList.add('examples-gallery__modal')

    const img = document.createElement('img')
    img.classList.add('examples-gallery__modal-img')
    img.src = this.images[0].getAttribute('data-origin') || this.images[0].src
    modal.appendChild(img)

    const close = document.createElement('button')
    close.classList.add('examples-gallery__modal-close')
    modal.appendChild(close)

    const nav = document.createElement('div')
    nav.classList.add('examples-gallery__modal-navigation')

    const buttonNext = document.createElement('button')
    buttonNext.classList.add('examples-gallery__modal-nav')
    buttonNext.classList.add('examples-gallery__modal-nav--next')
    nav.appendChild(buttonNext)

    const buttonPrev = document.createElement('button')
    buttonPrev.classList.add('examples-gallery__modal-nav')
    buttonPrev.classList.add('examples-gallery__modal-nav--prev')
    nav.appendChild(buttonPrev)
    modal.appendChild(nav)

    this.elem.appendChild(modal)
    this.modal = this.elem.querySelector('.examples-gallery__modal')
    
    this.modal.style.display = 'none'
    this.modal.style.opacity = '0'
    document.body.style.overflow = ''
    
    this.modalImage = this.modal.querySelector('.examples-gallery__modal-img')
    this.modal
      .querySelector('.examples-gallery__modal-close')
      .addEventListener('click', () => {
        this.deactiveModal()
      })

    this.modalNav = this.modal.querySelector('.examples-gallery__modal-navigation')
  }

  deactiveModal () {
    this.modal.style.opacity = '0'
    document.body.style.overflow = ''

    this.modal
      .addEventListener(
        'transitionend',
        () => this.modal.style.display = 'none',
        {once: true}
      )
  }

  activeModal (img, index) {
    this.modal.style.display = ''

    requestAnimationFrame(() => {
      requestAnimationFrame(() => {
        this.modalImage.src = img
        this.modal.style.opacity = '1'
        document.body.style.overflow = 'hidden'
      })
    })

    this.modalNav
      .addEventListener(
        'click',
        ({target}) => {
          let src = ''
          if (target.classList.contains('examples-gallery__modal-nav--prev') && index > 0) {
            let prevIndex = index - 1
            src = this.images[prevIndex].getAttribute('data-origin') || this.images[prevIndex].src
            this.activeModal(src, prevIndex)
            return
          } else if (target.classList.contains('examples-gallery__modal-nav--next') && index < this.images.length - 1) {
            let nextIndex = index + 1
            src = this.images[nextIndex].getAttribute('data-origin') || this.images[nextIndex].src
            this.activeModal(src, nextIndex)
            return
          }

          this.activeModal(img, index)
          return
        },
        {once: true}
      )
  }

  setPage (page = 1) {
    this.page = page
    const offset = page === 1 ? 0 : this.images[page - 1].offsetLeft
    this.outline.style.transform = `translateX(-${offset}px)`
    this.offset = offset
  }
}

window.addEventListener('load', () => {
  const sliders = Array.from(document.querySelectorAll('.certificate-slider'))

  sliders.forEach(slider => {
    try {
      const obj = new SertificateSlider(slider)
      obj.init()
    } catch(e) {
      console.log(e)
    }
  })
})
