
window.addEventListener('load', () => {
	const animatedScrollTo = require('animated-scrollto')
	let buttons = document.querySelectorAll('[data-hash]')
	if (buttons.length > 0) {
		buttons.forEach((item)=> {
			item.addEventListener('click', ()=>{
				let hash = item.dataset.hash
				let target = document.querySelector(`#${hash}`) ? document.querySelector(`#${hash}`) : false
				if (target) {
					animatedScrollTo(
						document.documentElement,
						target.getBoundingClientRect().top - 100,	
						1000,
						function () {/*callback*/}
					)
				} else {
					console.info('no such element scrollTo.js')
				}
			})
		})
	}
})