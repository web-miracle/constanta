const _ = {}
_.chunk = require('lodash/chunk')

class ExampleGallery {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.content = this.elem.querySelector('.examples-gallery__content')
    this.nav = this.elem.querySelector('.examples-gallery__navs')
    this.navInfo = this.nav.querySelector('.examples-gallery__nav-info')
    this.buttonPrev = this.nav.querySelector('[data-direction="prev"]')
    this.buttonNext = this.nav.querySelector('[data-direction="next"]')
    this.images = Array.from(this.elem.querySelectorAll('img'))
    this.pages = Math.ceil(this.images.length / 35)

    this.buildGallery()
    this.buildModal()
  }

  init () {
    this.setPage()

    this.buttonNext.addEventListener('click', () => {
      if (this.page === this.pages)
        return

      this.setPage(this.page + 1)
    })

    this.buttonPrev.addEventListener('click', () => {
      if (this.page === 1)
        return

      this.setPage(this.page - 1)
    })

    this.images.forEach((image, index) => {
      image.addEventListener('click', ({target}) => {
        const src = target.getAttribute('data-origin') || target.src
        this.activeModal(src, index)
      })
    })
  }

  buildGallery () {
    const slides = this.buildSlides()

    this.content.innerHTML = ''
    this.content.appendChild(slides)

    this.slides = Array.from(this.content.querySelectorAll('.examples-gallery__slide'))

    this.images.forEach(image => {
      image.classList.add('examples-gallery__img')
    })
  }

  buildModal () {
    const modal = document.createElement('div')
    modal.classList.add('examples-gallery__modal')

    const img = document.createElement('img')
    img.classList.add('examples-gallery__modal-img')
    img.src = this.images[0].getAttribute('data-origin') || this.images[0].src
    modal.appendChild(img)

    const close = document.createElement('button')
    close.classList.add('examples-gallery__modal-close')
    modal.appendChild(close)

    const nav = document.createElement('div')
    nav.classList.add('examples-gallery__modal-navigation')

    const buttonNext = document.createElement('button')
    buttonNext.classList.add('examples-gallery__modal-nav')
    buttonNext.classList.add('examples-gallery__modal-nav--next')
    nav.appendChild(buttonNext)

    const buttonPrev = document.createElement('button')
    buttonPrev.classList.add('examples-gallery__modal-nav')
    buttonPrev.classList.add('examples-gallery__modal-nav--prev')
    nav.appendChild(buttonPrev)
    modal.appendChild(nav)

    this.elem.appendChild(modal)
    this.modal = this.elem.querySelector('.examples-gallery__modal')
    
    this.modal.style.display = 'none'
    this.modal.style.opacity = '0'
    document.body.style.overflow = ''
    
    this.modalImage = this.modal.querySelector('.examples-gallery__modal-img')
    this.modal
      .querySelector('.examples-gallery__modal-close')
      .addEventListener('click', () => {
        this.deactiveModal()
      })

    this.modalNav = this.modal.querySelector('.examples-gallery__modal-navigation')
  }

  deactiveModal () {
    this.modal.style.opacity = '0'
    document.body.style.overflow = ''

    this.modal
      .addEventListener(
        'transitionend',
        () => this.modal.style.display = 'none',
        {once: true}
      )
  }

  activeModal (img, index) {
    this.modal.style.display = ''

    requestAnimationFrame(() => {
      requestAnimationFrame(() => {
        this.modalImage.src = img
        this.modal.style.opacity = '1'
        document.body.style.overflow = 'hidden'
      })
    })

    this.modalNav
      .addEventListener(
        'click',
        ({target}) => {
          let src = ''
          if (target.classList.contains('examples-gallery__modal-nav--prev') && index > 0) {
            let prevIndex = index - 1
            src = this.images[prevIndex].getAttribute('data-origin') || this.images[prevIndex].src
            this.activeModal(src, prevIndex)
            return
          } else if (target.classList.contains('examples-gallery__modal-nav--next') && index < this.images.length - 1) {
            let nextIndex = index + 1
            src = this.images[nextIndex].getAttribute('data-origin') || this.images[nextIndex].src
            this.activeModal(src, nextIndex)
            return
          }

          this.activeModal(img, index)
          return
        },
        {once: true}
      )
  }

  setPage (page = 1) {
    if (this.page === page)
      return

    this.page = page
    const info = `${this.page} из ${this.pages}`
    this.navInfo.innerText = info
    this.disableButton()
    this.activationSlide()
  }

  disableButton () {
    if (this.page === 1)
      this.buttonPrev.classList.add('examples-gallery__nav-link--disabled')
    else
      this.buttonPrev.classList.remove('examples-gallery__nav-link--disabled')

    if (this.page === this.pages)
      this.buttonNext.classList.add('examples-gallery__nav-link--disabled')
    else
      this.buttonNext.classList.remove('examples-gallery__nav-link--disabled')
  }

  activationSlide () {
    this.slides.forEach(slide => {
      slide.style.transform = `translateX(-${(this.page - 1) * 100}%)`
    })
  }

  buildSlides () {
    const chunk = _.chunk(this.images, 35)

    const fragment = document.createDocumentFragment();

    chunk.forEach(images => {
      const slide = document.createElement('div')
      slide.classList.add('examples-gallery__slide')

      images.forEach(image => {
        slide.appendChild(image)
      })

      fragment.appendChild(slide)
    })

    return fragment
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const galleries = Array.from(document.querySelectorAll('.examples-gallery'))

  galleries.forEach(gallery => {
    try {
      const obj = new ExampleGallery(gallery)
      obj.init()
    } catch(e) {
      console.log(e)
    }
  })
})
