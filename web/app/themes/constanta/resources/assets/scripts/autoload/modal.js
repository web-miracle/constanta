class Modal {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.dataName = this.elem.getAttribute('data-modal')
    this.modal = document.querySelector(`div[data-modal='${this.dataName}']`)
    this.closeBtn = this.modal.querySelector('.modal__close')
  }

  init () {
    let callback = this.open.bind(this)
    this.elem.addEventListener('click', callback)
    let callback1 = this.close.bind(this)
    this.closeBtn.addEventListener('click', callback1)
  }

  open () {
    this.modal.style.display = 'block'
    document.body.style.overflow = 'hidden'

    requestAnimationFrame(() => {
      requestAnimationFrame(() => {
        this.modal.style.opacity = '1'
      })
    })

    this.closeWrap()
  }

  close () {
    this.modal.style.opacity = '0'
    this.modal.addEventListener('transitionend', () => {
      document.body.style.overflow = 'auto'
      this.modal.style.display = 'none'
    }, {once: true})   
  }

  closeWrap () {
    this.modal.addEventListener('click', (e) => {
      if (e.target === this.modal) {
        this.close()
        return
      }
      this.closeWrap()
    }, {once: true})
  }
}


window.addEventListener('load', () => {
  const buttonsModal = Array.from(document.querySelectorAll('button[data-modal]'))

  buttonsModal.forEach(button => {
    try {
      let modalInitObj = new Modal(button)
      modalInitObj.init()
    } catch(e) {
      console.log(e)
    }
  })
})
