const _ = {}
_.chunk = require('lodash/chunk')

class BlogSlider {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.content = this.elem.querySelector('.blog-section__content')
    this.pagenation = this.elem.querySelector('.blog-pagination')
    this.slides = Array.from(this.elem.querySelectorAll('.blog-item'))
    this.pages = this.setPages()

    this.slideCardWidth()

    //TODO: resize control (landscape)

    this.buildNavs()
  }

  init () {
    this.setPage()

    this.navs.forEach(nav => {
      nav.addEventListener('click', () => {
        const page = nav.getAttribute('data-page')
        this.setPage(page)
      })
    })

    this.interval = setInterval(() => {
      if (this.page === this.pages)
        this.setPage()
      else{
        const item = parseInt(this.page, 10) + 1
        this.setPage(item)
      }
    }, 8000)
  }

  setPages () {

    const width = document.body.clientWidth
    if (width > 1200) 
      return Math.ceil(this.slides.length / 4)
    
    if (width > 767) 
      return Math.ceil(this.slides.length / 2)
    
    return this.slides.length

  }

  slideCardWidth () {
    const rem = parseFloat(getComputedStyle(document.documentElement).fontSize)
    this.slideMargin = 3 * rem
    const width = document.body.clientWidth
    if (width > 1200) {
      this.slideWidth = 28 * rem
      this.slideOffset = 4 * (this.slideWidth + this.slideMargin)
    }
    else if (width > 767) {
      this.slideWidth = 28 * rem
      this.slideOffset = 2 * (this.slideWidth + this.slideMargin)
    }
    else {
      this.slideWidth = 22 * rem
      this.slideOffset = this.slideWidth + this.slideMargin
    }

  }

  buildNavs () {
    this.pagenation.innerHTML = ''

    for (let i = 0; i < this.pages; i++) {
      let dot = document.createElement('div')
      dot.classList.add('blog-pagination__item')
      dot.setAttribute("data-page", i + 1)

      this.pagenation.appendChild(dot)
    }

    this.navs = Array.from(this.pagenation.querySelectorAll('.blog-pagination__item'))
  }

  setPage (page = 1) {
    if (this.page === page || this.pages < page)
      return

    this.page = page
    this.moveContent()
    this.activeSlides()
    this.activationNav()
  }

  moveContent () {
    const move = this.slideOffset * (this.page - 1)
    this.content.style.transform = `translateX(-${move}px)`
  }

  activeSlides () {
    this.slides.forEach((slide, index) => {
      const width = document.body.clientWidth

      if (width > 1200) {
        if (this.page - 1 === Math.floor(index / 4))
          slide.classList.add('blog-item_active')
        else
          slide.classList.remove('blog-item_active')
      }
      else if (width > 767) {
        if (this.page - 1 === Math.floor(index / 2))
          slide.classList.add('blog-item_active')
        else
          slide.classList.remove('blog-item_active')
      }
      else {
        if (this.page - 1 === index)
          slide.classList.add('blog-item_active')
        else
          slide.classList.remove('blog-item_active')
      }
    })
  }

  activationNav () {
    this.navs.forEach(nav => {
      nav.classList.remove('blog-pagination__item_active')
    })

    this.navs[this.page - 1].classList.add('blog-pagination__item_active')
  }
}

window.addEventListener('load', () => {
  const post_sliders = Array.from(document.querySelectorAll('.blog-section'))

  post_sliders.forEach(slider => {
    try {
      const obj = new BlogSlider(slider)
      obj.init()
    } catch(e) {
      console.log(e)
    }
  })
})
