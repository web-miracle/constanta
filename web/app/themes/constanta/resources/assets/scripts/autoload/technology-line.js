class TechSvgLine {
  constructor () {
    this.section = document.querySelector('.technology')
    this.tech_cards = Array.from(this.section.querySelectorAll('.technology__technology-card:nth-of-type(4n+1)'))
    this.svg_lines = Array.from(this.section.querySelectorAll('.technology__svg-line'))
  }

  init () {
    this.svg_lines.forEach((line, index) => {
      const top = this.tech_cards[index].offsetTop
      line.style.top = `calc(${top}px - 4.5rem)`

      setTimeout(() => {
        line.classList.add('technology__svg-line--animation')
      }, 6000 * index)
    })
  }
}

window.addEventListener('load', () => {
  try {
    let initObj = new TechSvgLine()
    initObj.init()
  } catch(e) {
    console.log(e)
  }
})
