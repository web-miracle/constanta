/* eslint-disable */
const _ = {}
_.find = require('lodash/find')
_.reduce= require('lodash/reduce')

class Calculator {
	constructor (navs, table) {
		// if (!(navs instanceof Node)) throw Error(navs + ' - isn`t element of DOM')
		// if (!(table instanceof Node)) throw Error(table + ' - isn`t element of DOM')
		
		this.navs = navs
		this.table = table
		this.data = []

		this.setCountLogic()
	}

	init () {
		try	{
			this.navs.forEach(item => {
				this.parseData(item)
				item.addEventListener('click', e => {
					this.setActiveCat(e.target.dataset.cat)
				})
			})			
			this.setActiveCat(0)
		} catch(e) {
			console.log('Init calc: ', e)
		}
	}

	parseData(item) {
		const rows = Array.from(this.table.querySelector(`#tbody_${item.dataset.cat}`).children)
		let cat_name = item.innerText
		this.data.push({
			'cat': {
				'name': cat_name,
				'id': item.dataset.cat,
			},
			'products': [],
			'active': false,
			'total': 0, 
		})
		if (rows) {
			rows.forEach(item => {
				this.data[this.data.length - 1].products.push({
						'code': item.querySelector('.jCode').innerHTML,
						'name' : item.querySelector('.jName').innerHTML,
						'duration' : item.querySelector('.jDuration').innerHTML,
						'price' : item.querySelector('.jPrice').innerHTML,
						'count' : 0,
						'summ' : 0,
					}
				)
			})
		}
	}

	setCountLogic() {
		const minus = this.table.querySelectorAll('.jMinus')
		minus.forEach(element => {
			element.addEventListener('click', () => {
				let activeObj = _.find(this.data, ['active', true])
				this.countChange(element.parentElement.parentElement.dataset.id, 'minus', activeObj)
				this.setTotal(activeObj)
				this.setTotalWithDiscount(activeObj)
				this.printDataOnPage(this.data)
			})
		})
		const plus = this.table.querySelectorAll('.jPlus')
		plus.forEach(element => {
			element.addEventListener('click', () => {
				let activeObj = _.find(this.data, ['active', true])
				this.countChange(element.parentElement.parentElement.dataset.id, 'plus', activeObj)
				this.setTotal(activeObj)
				this.setTotalWithDiscount(activeObj)
				this.printDataOnPage(this.data)
			})
		})
	}

	printDataOnPage(data) {
		let el = document.querySelector('div#data') ? document.querySelector('div#data') : document.createElement('div')
		if (el.id.length === 0) el.id = 'data'
		el.innerHTML = JSON.stringify(data)
		document.body.appendChild(el)
	}

	setTotalWithDiscount(obj) {
		let disc = document.querySelectorAll('.jDiscount')
		if (disc) {
			disc.forEach(item => {
				let total = item.parentElement.querySelector('.jDiscTotal')
				total.innerHTML = Math.floor(obj.total * (100 - parseInt(item.innerHTML)) / 100)
			})
		}
	}

	setTotal (obj) {
		let total = 0
		obj.products.forEach(item => {
			total += item.summ
		})
		obj.total = total

		document.querySelector('.jTotal').innerHTML = total
	}

	countChange(rowId, operand, obj) {
		if (operand === 'plus') obj.products[rowId].count++
		else if (obj.products[rowId].count > 0) obj.products[rowId].count--
		obj.products[rowId].summ = obj.products[rowId].count * obj.products[rowId].price
		
		let rows = Array.from(this.table.querySelector(`#tbody_${obj.cat.id}`).querySelectorAll('.jRow'))
		let row = rows[rowId]
		row.querySelector('.jCount').innerHTML = obj.products[rowId].count
		row.querySelector('.jSumm').innerHTML = obj.products[rowId].summ
		if (obj.products[rowId].count === 0) row.querySelector('.jMinus').classList.add('table__minus_disabled')
		else row.querySelector('.jMinus').classList.remove('table__minus_disabled')
	}
	
	setActiveCat(id = 0) {
		try {
			const tbody = this.table.querySelector(`#tbody_${id}`)
			const tbodys = this.table.querySelectorAll('.jTbody')
			tbodys.forEach(item => item.classList.remove('table__tbody_active'))
			this.navs.forEach(item => item.classList.remove('categories__item_active'))
			this.navs[id].classList.add('categories__item_active')
			tbody.classList.add('table__tbody_active')
			this.data.forEach(item => item.active = false )
			this.data[id].active = true
			this.setTotal(this.data[id])
			this.setTotalWithDiscount(this.data[id])
		} catch (e) {
			console.log('Пытаемся установить активный элемент. Ошибка:' + e)
		}

	}
}
window.addEventListener('load', () => {
	try {
		const navs = Array.from(document.querySelectorAll('.jCategories')[0].children)
		// console.log(navs)
		const table = document.querySelectorAll('.jTable')[0]
		const calc = new Calculator(navs, table)
		calc.init()
	} catch (e) {
		console.log(e)
	}
})