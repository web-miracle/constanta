/* eslint-disable */
class Forms {
	constructor (forms) {
		this.forms = forms
		this.action = 'send_mail'
	}

	init () {
		this.forms.forEach(item => {
			item.addEventListener('submit', e => {
				e.preventDefault()
				this.wait(e.target)
				let formName = item.querySelector('.jFormTitle') ? item.querySelector('.jFormTitle').innerText : ''
				let name = item.querySelector('.jName') ? item.querySelector('.jName').value : ''
				let tel = item.querySelector('.jTel') ? item.querySelector('.jTel').value : ''
				let addData = item.querySelector('.jData') ? item.querySelector('.jData').value : ''
				if (e.target.parentElement.parentElement.dataset.modal === 'calc') addData = JSON.parse(document.querySelector('#data').innerHTML).filter(item => item.active)
				
				if (name === '' || tel === '') return
				const data = {
					action: this.action,
					name: name,
					tel: tel,
					data: addData,
					formName: formName,
					nonce: window.myajax.nonce
				}

				jQuery.post(window.myajax.url, data, response => {
					if (!response) this.fail()
					let ans = JSON.parse(response)
					if (ans.some((item) => item )) this.success(e.target)
					else this.fail()
				  }).fail(() => this.fail())
			})
		})
	}
	
	wait (item) {
		item.querySelector('button').setAttribute('disabled', true)
	}

	success(item) {
		item.querySelector('button').removeAttribute('disabled')
		alert('Данные отправлены')
	}

	fail() {
		console.log('fail')
	}
}

window.addEventListener('load', () => {
	try {
		const forms = Array.from(document.querySelectorAll('form'))
		const newForms = new Forms(forms)
		newForms.init()
	} catch (e) {
		console.log('Инициализация обработки форм: ', e)
	}
})