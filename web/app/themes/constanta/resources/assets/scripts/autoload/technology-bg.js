class TechmologyBG {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
  }

  init () {
    const circles = this.buildCircle()

    this.elem.appendChild(circles)
    this.circle = Array.from(this.elem.querySelectorAll('.technology__bg-circle'))
  }

  buildCircle () {
    let iteration = 10

    const fragment = document.createDocumentFragment()

    while (iteration > 0) {
      let circle = document.createElement('i')
      circle.classList.add('technology__bg-circle')
      let size = Math.random() * (22.5 - 3) + 3
      let top = Math.random() * 100
      let left = Math.random() * 100
      circle.style.height = `${size}rem`
      circle.style.width = `${size}rem`
      circle.style.top = `${top}%`
      circle.style.left = `${left}%`

      fragment.appendChild(circle)
      iteration--
    }

    return fragment
  }
}

window.addEventListener('load', () => {
  try {
    const section = document.querySelector('.technology')
    let initObj = new TechmologyBG(section)
    initObj.init()
  } catch(e) {
    console.log(e)
  }
})
