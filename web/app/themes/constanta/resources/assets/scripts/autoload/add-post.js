/* eslint-disable */

class PostAdded {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.elem = node
    this.action = "add_posts"

    this.addButton = this.elem.querySelector('[data-post-add]')
    this.content = this.elem.querySelector('.blog-page__reports')
    this.offset = this.content.querySelectorAll('.blog-item').length
    this.per_page = 6
    this.tag = this.elem.getAttribute('data-tag')

    if (this.offset < 1)
      this.addButton.style.display = 'none'
  }

  init () {
    this.addButton.addEventListener('click', () => {
      this.request();
    })
  }

  request () {
    this.wait()
    const data = {
      action: this.action,
      offset: this.offset,
      per_page: this.per_page,
	  tags: this.tag,
	  nonce: window.myajax.nonce
    }

    jQuery.post(window.myajax.url, data, response => {
      if (!response)
        this.fail()

      const div = document.createElement('div')
      div.innerHTML = response.trim()

      const cards = Array.from(div.children)

      cards.forEach(card => {
        this.offset++
        this.content.appendChild(card)
      })

      this.success()
    }).fail(() => this.fail())
  }

  wait () {
    this.addButton.setAttribute('disabled', true)
  }

  fail () {
    this.addButton.style.opacity = '0'

    setTimeout(() => this.addButton.style.display = 'none', 1000)
  }

  success () {
    this.addButton.removeAttribute('disabled')
  }
}

window.addEventListener('load', () => {
  const page = document.querySelector('.blog-page')

  try {
    let pageObj = new PostAdded(page)
    pageObj.init()
  } catch(e) {
    console.log(e)
  }
})
