class Menu {
  constructor (node) {
    if (!(node instanceof Node))
      throw Error(node + ' - isn`t element of DOM')

    this.element = node
    this.gamburger = this.element.querySelector('.menu__gamburger')
    this.dropList = this.element.querySelector('.menu__drop')
  }

  init () {
    this.gamburger.addEventListener('click', () => {
      if (!this.open)
        this.close()

      this.open = !this.open
      this.toggle()
    })
  }

  close() {
    document.body.addEventListener('click', ({target, path}) => {
      if (!this.open)
        return

      if (path.indexOf(this.element) !== -1 || target === this.gamburger) {
        this.close()
      } else {
        this.open = false
        this.toggle()
      }
    }, {once: true})
  }

  toggle () {
    this.gamburger.classList.toggle('menu__gamburger--active')
    this.dropList.classList.toggle('menu__drop--active')
  }
}

window.addEventListener('load', () => {
  const menus = document.querySelectorAll('.menu')

  menus.forEach(menu => {
    try {
      let menuInitObject = new Menu(menu)
      menuInitObject.init()
    } catch(e) {
      console.log(e)
    }
  })
})